import React, { Component } from 'react'
import { Router, Route, Switch } from "react-router-dom"
import Home from './pages/home'
import HomeOrgao from './pages/home_orgao'
import Login from './pages/login'
import StaticRoute from './components/static_route'
import NovosColaboradores from './pages/novos_colaboradores'
import Galeria from './pages/galeria'
import Post from './pages/post'
import PostVideo from './pages/post_video'
import FaleConosco from './pages/fale_conosco'
import Adf from './pages/adf'
import AdfDocsEspecificos from './pages/adf_docs_especificos'
import './sass/app.scss'
import OrgaoContext from './context'
import ReactGA from 'react-ga';
import createBrowserHistory from 'history/createBrowserHistory'

const history = createBrowserHistory()
ReactGA.initialize('UA-145068567-1');
history.listen( (location, action) => {
  ReactGA.pageview(location.pathname + location.search);
  console.log("$LOCATION", location.pathname + location.search);
})

class App extends Component {
  constructor(props){
    super(props)

    // State com todos os dados globais necessários para a aplicação
    this.state = {
      orgao: "inema",
      pagina: "",
      alterarOrgao: e => {        
        if(e){
          let orgao = e.target.dataset.orgao;
          if(this.state.orgao !== orgao) this.setState({ 
            orgao: orgao,
          }, this.redirect)
        }else{
          this.state.orgao === "inema" ? this.setState({ orgao: "sema" }) : this.setState({ orgao: "inema" })
        }
      },

      /* ---------
        Armazenamento de objeto routeProps em contet para poder utilizar history.push()
      ---------- */

      routeProps: {},
      changeRouteProps: routeProps => {
        if(this.state.routeProps !== routeProps) this.setState({ routeProps }, () => console.log(this.state.routeProps))
      },

      /* ---------
        Dados de página atual dinâmica a ser renderizada
      ---------- */

      json_data: {
        id: 0,
        title: {
          rendered : "Carregando título..."
        },
        content: {
          rendered: "Carregando conteúdo..."
        }
      },
      updateJsonData: json_data => this.setState({ json_data })

    };
  }

  redirect = () => {
    let routeProps = this.state.routeProps;
    // Verifica se o órgao match é o mesmo órgao state
    if(routeProps){
      if(routeProps.match.params.orgao !== this.state.orgao){
        // Atualiza url        
        let newRouteProps = this.state.routeProps;
        newRouteProps.match.url = newRouteProps.match.url.replace(newRouteProps.match.params.orgao, this.state.orgao);
        // Atualiza orgao
        newRouteProps.match.params.orgao = this.state.orgao;
        // Encaminha para nova url
        this.setState({ routeProps : newRouteProps }, () => this.state.routeProps.history.push(this.state.routeProps.match.url))
      }
    }
  }

  render() {
    return (
    <Router history={history}>
      <OrgaoContext.Provider value={this.state} >
      <Switch>
        <Route exact path="/login" component={Login} />  
        <Route exact path="/" component={Home} />
        <Route exact path="/:orgao" component={HomeOrgao} />
        <Route exact path="/:orgao/galeria" component={(props) => <Galeria routeProps={props}/>} />
        <Route exact path="/:orgao/noticias/:slug?" component={
          (props) => <Post 
          routeProps={props}
          cpt="posts" // noticias ficou com o nome padrão
          sidebar
          />
          } />
        <Route exact path="/:orgao/ver-de-dentro/:slug?" component={
          (props) => <Post 
          routeProps={props}
          cpt="ver-de-dentro"
          />
          } />
        <Route exact path="/:orgao/altos-papos/:slug?" component={
          (props) => <Post 
          routeProps={props}
          cpt="altos-papos"
          />
          } />
        <Route exact path="/:orgao/vem-ai/:slug?" component={
          (props) => <Post 
          routeProps={props}
          cpt="vem-ai"
          />
          } />
        <Route exact path="/:orgao/destaque-do-mes/:slug?" component={
          (props) => <Post 
          routeProps={props}
          cpt="destaque-do-mes"
          />
          } />
        <Route exact path="/:orgao/fica-a-dica/:slug?" component={
          (props) => <Post 
          routeProps={props}
          cpt="fica-a-dica"
          />
          } />
        <Route exact path="/:orgao/videos/:slug?" component={
          (props) => <PostVideo 
          routeProps={props}
          cpt="videos"
          />
          } />
        <Route exact path="/:orgao/novos-colaboradores/:id?" component={(props) => <NovosColaboradores routeProps={props}/>} />
        <Route exact path="/:orgao/fale-conosco" component={(props) => <FaleConosco routeProps={props}/>} />
        <Route exact path="/:orgao/adf/legislacao-especifica" component={(props) => <AdfDocsEspecificos routeProps={props}/>} />
        <Route exact path="/:orgao/adf/:aba?/:ano?" component={(props) => <Adf routeProps={props}/>} />
        <OrgaoContext.Consumer>
        { context =>  <Route exact path="/:orgao/:pagina" component={(props) => <StaticRoute context={context} routeProps={props}/> } /> }
        </OrgaoContext.Consumer>
      </Switch>
      </OrgaoContext.Provider>
    </Router>
    );
  }
}

export default App;
