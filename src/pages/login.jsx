import React, { Component } from 'react';
import Reaptcha from 'reaptcha';
import InputText from "../components/form/input_text";
import InputPass from "../components/form/input_pass";
import Select from "../components/form/input_select";
import Button from "../components/form/button";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      verified: false
    };
  }
  
  onVerify = () => {
    this.setState({
      verified: true
    });
    console.log("verified");
  };

  componentDidMount(){
    console.log("width", window.innerWidth);    
  }

  onChange = (value) => {
    console.log("Captcha value:", value);
  }

  render() {
    return (
      <div className="login_page">
        <main>
          <header>
            <h2>INTRANET DO MEIO AMBIENTE</h2>
          </header>
          <section>
            <form>
              <InputText placeholder="Login" id="login"/>
              <InputPass placeholder="Senha" id="senha"/>
              <Select placeholder="Órgão" id="orgao">
                <option>INEMA</option>
                <option>SEMA</option>
              </Select>
              { window.innerWidth < 767.98 ?
                <Reaptcha 
                sitekey="6LfqHpsUAAAAAAFJE9LCDFf6yxcoaEmVz7zvbx3R" 
                onVerify={this.onVerify}
                size="compact"
                /> :
                <Reaptcha 
                sitekey="6LfqHpsUAAAAAAFJE9LCDFf6yxcoaEmVz7zvbx3R" 
                onVerify={this.onVerify} />
              }
              <Button type="submit" className="btn __full mt-30" disabled={!this.state.verified}>ENTRAR</Button>
              <p>
                Para ter acesso a Intranet, utilize login e senha da rede<br/>
                Dúvidas ou problemas? Envie um e-mail para: <a href="mailto:intranet.sema@sema.ba.gov.br">intranet.sema@sema.ba.gov.br</a>
              </p>
            </form>
          </section>
        </main>
        <footer>
          <img src="/img/logo_inema.jpg"/>
          <img src="/img/logo_governo_bahia.jpg"/>
        </footer>
      </div>
    );
  }
}

export default Login;

// import React, { Component } from 'react';
// import Reaptcha from 'reaptcha';

// export default class MyForm extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       verified: false
//     };
//   }

//   onVerify = () => {
//     this.setState({
//       verified: true
//     });
//   };

//   render() {
//     return (
//       <form>
//         <Reaptcha sitekey="6LfqHpsUAAAAAAFJE9LCDFf6yxcoaEmVz7zvbx3R" onVerify={this.onVerify} />
//         <button type="submit" disabled={!this.state.verified}>
//           Submit
//         </button>
//       </form>
//     );
//   }
// }