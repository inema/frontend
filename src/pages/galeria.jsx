import React, { Component } from 'react'
import OrgaoContext from '../context'
import LayoutSemBanner from '../layout/sem_banner'
import MenuCaminho from '../components/menu_caminho'
import ImageGallery from 'react-image-gallery';
import "react-image-gallery/styles/css/image-gallery.css";
import Modal from '@material-ui/core/Modal';

export default class Galeria extends Component {

  static contextType = OrgaoContext;

  constructor(props){
    super(props)

    this.state = {
      galeria: {
        albuns: [],
        album_selecionado: {
          imagens: [
            { 
              original: null,
              thumbnail: null
            }
          ]
        }
      }, 
      modalIsOpened: false
    }
  }

  loadGalerias = () => {
    fetch(`${process.env.REACT_APP_WP_API}/wp-json/galerias/v1/albuns/`).then(
      (response) => response.json() ).then(
        (r) => {
          const galeria = this.state.galeria
          galeria.albuns = r
          this.setState({ galeria }, console.log(this.state.galeria.albuns));
      })
  }

  handleOpen = (e) => {
    e.preventDefault();
    const id = e.currentTarget.dataset.id
    const galeria = this.state.galeria
    const albuns = galeria.albuns
    albuns.forEach( v => {
      if(v.id === id){
        galeria.album_selecionado = v
      }
    })
    console.log(galeria)
    this.setState({ galeria, modalIsOpened: true });
  };

  handleClose = () => {
    this.setState({ modalIsOpened: false });
  };

  componentDidMount(){
    const context = this.context;
    const props = this.props;

    this.loadGalerias();

    if(context.routeProps.location !== props.routeProps.location){
      context.changeRouteProps(props.routeProps)
    }
  }

  render(){
    const context = this.context;

    return (
      <LayoutSemBanner>
        <MenuCaminho/>
        <h2>Galeria</h2>
          <section id="galeria_lista">
            {this.state.galeria.albuns.map( v => (
              <div onClick={this.handleOpen} data-id={v.id}>
                <img src={v.capa.url} alt={v.titulo}/>
                <div className={`cortina __${context.orgao}`}></div>
                <span>{v.titulo}</span>
              </div>
            ))}
          </section>
          <Modal
            id="modal_galeria" 
            open={this.state.modalIsOpened}
            onClose={this.handleClose}
          >
            <ImageGallery items={this.state.galeria.album_selecionado.imagens} />
          </Modal>
      </LayoutSemBanner>
    )
  }
}