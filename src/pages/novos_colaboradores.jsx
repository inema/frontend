import React, {Component} from 'react'
import OrgaoContext from '../context'
import LayoutSemBanner from '../layout/sem_banner'
import MenuCaminho from '../components/menu_caminho'
import WidgetNovosColaboradores from '../components/widget/novos_colaboradores';
import ReactHtmlParser from 'react-html-parser';
import { Link } from "react-router-dom"
import { PaginacaoVoltar, PaginacaoAvancar } from "../components/own_icons"

export default class NovosColaboradores extends Component {

  static contextType = OrgaoContext;
  
  constructor(props){
    super(props)

    this.state = {
      colaboradores : [],
      colaborador : {
        nome: null,
        foto: {
          path: "teste"
        },
        biografia: null
      },
      id : null,
      historico: false,
      pagina_atual: 1,
      paginas: 0,
      itens_por_pagina: 5,
      colaboradoresMostrados: []
    }
  }

  loadColaborador = id => {
    console.log(`${process.env.REACT_APP_WP_API}/wp-json/colaboradores/v1/colaboradores`);
    fetch(`${process.env.REACT_APP_WP_API}/wp-json/colaboradores/v1/colaboradores/${id}`).then(
      (response) => {
        // console.log(response.headers.get("X-WP-Total"))
        // this.setState({ paginas: parseInt(response.headers.get("X-WP-TotalPages")) });
        return response.json() 
      } ).then(
        (r) => {
          console.log(r);
          this.setState({ colaborador : r });
          console.log(this.state);
      })
  }

  loadColaboradores = () => {
    console.log(`${process.env.REACT_APP_WP_API}/wp-json/colaboradores/v1/colaboradores`);
    fetch(`${process.env.REACT_APP_WP_API}/wp-json/colaboradores/v1/colaboradores`).then(
      (response) => {
        // console.log(response.headers.get("X-WP-Total"))
        // this.setState({ paginas: parseInt(response.headers.get("X-WP-TotalPages")) });
        return response.json() 
      } ).then(
        (r) => {
          console.log(r);
          this.setState({ 
            colaboradores : r,
            paginas: Math.ceil(r.length / this.state.itens_por_pagina)
          }, () => this.mostrarColaboradores() );
          console.log(this.state);
      })
  }

  componentDidMount(){
    if(this.context.routeProps.location !== this.props.routeProps.location){
      this.context.changeRouteProps(this.props.routeProps)
    }

    // id pode ser historico ou algum id em específico
    let params = this.props.routeProps.match.params
    console.log(this.props.routeProps.match.params)

    console.log("mid", parseInt(params.id))
    // this.mostrarColaborador();
    if(parseInt(params.id) > 0){
      this.loadColaborador(params.id)
    }else{
      // se escrever algo tipo 'historico' retornará NaN que não é > 0
      this.loadColaboradores()
      this.setState({ 
        historico : true
      })
    }
  }
  
  componentDidUpdate(prevProps, prevState){
    let params = this.props.routeProps.match.params
    if(prevProps.routeProps.match.params.id !== params.id){
      this.loadColaborador(params.id)
      if(!parseInt(params.id) > 0){
        this.loadColaboradores()
        this.setState({ 
          historico : true
        })
      }
    }
  }

  paginas = () => {
    const paginas = [];
    // O objetivo é mostrar apenas cinco páginas, duas antes e duas depois da atual caso não sejam as primeiras nem as últimas
    for (let i = 1; i <= this.state.paginas; i++) {
      paginas.push(i)            
    }
    return paginas;
  }

  /* ----------
    função que captura os itens que devem ser mostrados e seta o state tomando por base o número de itens_por_pagina. Ou seja, se eu tenho 20 itens total e o n de itens_por_pagina é 5 eu separo em 5 e vejo quais serão mostrados agora dependedo da página atual
  ---------- */

  mostrarColaboradores = () => {
    // Selecionar colaboradores a serem mostrados por paginação
    let pagina_atual = this.state.pagina_atual - 1
    // indexes a serem mostrados
    let indexes = []
    for (let i=pagina_atual*5; i < (pagina_atual*5+this.state.itens_por_pagina); i++) {
      indexes.push(i)
    }
    let array_retorno = []
    this.state.colaboradores.forEach( (e, i) => {
      if(indexes.includes(i)){
        array_retorno.push( e )
      }
    })
    console.log(array_retorno)

    this.setState({ colaboradoresMostrados : array_retorno })
  }
  
  /* ----------
    função para retorno de página no onClick do arrowBack
  ---------- */

  voltarPagina = () => {
    // Verifica se é a primeira página
    if(this.state.pagina_atual === 1){
      return true
    }else{
      this.setState({ pagina_atual : this.state.pagina_atual-1 }, () => this.mostrarColaboradores())
    }
  }

  avancarPagina = () => {
    // Verifica se é a última página
    if(this.state.pagina_atual === this.state.paginas){
      return true
    }else{
      this.setState({ pagina_atual : this.state.pagina_atual+1 }, () => this.mostrarColaboradores())
    }
  }

  /* ----------
    função que especifica para qual página vai com o parâmetro do número da página
  ---------- */

  paraPagina = pag => e => {
    this.setState({ pagina_atual : pag }, () => this.mostrarColaboradores())
  }

  render(){
    if(!this.state.historico){
    return (
      <LayoutSemBanner>
        <MenuCaminho/>
          <h2>Colaboradores</h2>
          <section id="colaborador" className="row">
            <div style={{ backgroundImage: `url("${this.state.colaborador.foto.path}")` }} className="foto_perfil col-md-3"/>
            <main className="col-md-9">
              <h4>{this.state.colaborador.nome}</h4>
              <div>{ReactHtmlParser(this.state.colaborador.biografia)}</div>
            </main>
          </section>
          <h2>Novos Colaboradores</h2>
          <WidgetNovosColaboradores noTitle allowClick className="col-md-12" onClick={this.mostrarColaborador}/>
      </LayoutSemBanner>
    )}else{
      // Mostra histórico de Notícias
      return (
        <LayoutSemBanner>
          <MenuCaminho/>
          <h2>Colaboradores</h2>
            <section id="historico_posts" className="conteudo custom">
              {this.state.colaboradoresMostrados.map( (v, i, a) => {
                console.log("$$$", v.foto)
                let titulo = v.nome,
                dataAdmObj = new Date(Date.parse(v.data_admissao)),
                data = dataAdmObj.getDate()+"/"+dataAdmObj.getMonth()+"/"+dataAdmObj.getFullYear(),
                trecho = v.biografia.length > 100 ? `${v.biografia.substring(0, 97)} [...]` : v.biografia;

                return (
                  <Link to={`/${this.context.orgao}/novos-colaboradores/${v.id}`} onClick={this.forceUpdate}>
                  <div className="posts_item_historico">
                    {v.foto ? <div className="img_destaque" style={{ backgroundImage: `url("${v.foto.path}")` }}/> : null}
                    <div className="conteudo">
                      <span className="data">{data}</span>
                      <h4>{titulo}</h4>
                      <div className="trecho">{ReactHtmlParser(trecho)}</div>
                    </div>
                  </div>
                  </Link>
                )
                
              })}
              <div className="paginacao">
                <button onClick={this.voltarPagina}><PaginacaoVoltar/></button>
                {this.paginas().map( v => {
                  let cssClass = v === this.state.pagina_atual ? "atual" : null
                  return <button className={`${cssClass} __${this.context.orgao}`} onClick={this.paraPagina(v)}>{v}</button>
                })}
                <button onClick={this.avancarPagina}><PaginacaoAvancar/></button>
              </div>
            </section>
        </LayoutSemBanner>
      )
    }
  }
}