import React, { Component } from 'react'
import OrgaoContext from '../context'
import LayoutSemBanner from '../layout/sem_banner'
import MenuCaminho from '../components/menu_caminho'
import ReactHtmlParser from 'react-html-parser';
import { PaginacaoVoltar, PaginacaoAvancar } from "../components/own_icons"
import ReactPlayer from 'react-player'
import Modal from '@material-ui/core/Modal';


/**
 * Código realmente feito como cópia do post e às pressas por se tratar de um tipo de post de comportamento totalmente diferente e feito como plus por solicitação de última hora que não estava no backlog
 */

export default class Posts extends Component {

  static contextType = OrgaoContext;

  constructor(props) {
    super(props)

    this.state = {
      post: {},
      posts: [],
      paginas: 1,
      pagina_atual: 1,
      modalIsOpened: false
    }
  }

  /* ----------
    Função para carregar os posts
  ---------- */
  loadPosts = () => {
    fetch(`${process.env.REACT_APP_WP_API}/wp-json/wp/v2/videos?per_page=5&page=${this.state.pagina_atual}`).then(
      (response) => {
        // console.log(response.headers.get("X-WP-Total"))
        this.setState({ paginas: parseInt(response.headers.get("X-WP-TotalPages")) });
        return response.json()
      }).then(
        (r) => {
          console.log("$$$$", r);
          this.setState({ posts: r });
          console.log(this.state);
        })
  }

  paginas = () => {
    const paginas = [];
    // O objetivo é mostrar apenas cinco páginas, duas antes e duas depois da atual caso não sejam as primeiras nem as últimas
    for (let i = 1; i <= this.state.paginas; i++) {
      paginas.push(i)
    }
    return paginas;
  }

  voltarPagina = () => {
    // Verifica se é a primeira página
    if (this.state.pagina_atual === 1) {
      return true
    } else {
      this.setState({ pagina_atual: this.state.pagina_atual - 1 })
    }
  }

  avancarPagina = () => {
    // Verifica se é a última página
    if (this.state.pagina_atual === this.state.paginas) {
      return true
    } else {
      this.setState({ pagina_atual: this.state.pagina_atual + 1 }, () => console.log("pagina atual: ", this.state.pagina_atual))
    }
  }

  paraPagina = pag => e => {
    this.setState({ pagina_atual: pag }, () => console.log("pagina atual: ", this.state.pagina_atual))
  }

  showVideo = videoPost => e => {
    console.log("$$$$", videoPost, e)
    this.setState({ post: videoPost }, this.handleOpen )
  }

  handleOpen = () => {
    this.setState({ modalIsOpened: true });
  };

  handleClose = () => {
    this.setState({ modalIsOpened: false });
  };

  componentDidMount(){
    this.loadPosts()
  }

  render() {

    return [
      <Modal
        id="modal_widget_videos" 
        open={this.state.modalIsOpened}
        onClose={this.handleClose}
        >
        {this.state.post.id ? 
        <div class="modal_video_box">
        <ReactPlayer
          url={this.state.post.meta._video_url}
          controls={true}
        /></div> : <p>Sem vídeo</p> }
      </Modal>,
      <LayoutSemBanner>
        <MenuCaminho />
        <h2>Vídeos</h2>
        <section id="historico_posts" className="conteudo">
          {this.state.posts.length ? this.state.posts.map(v => {
            let titulo = v.title.rendered,
              dataObj = new Date(Date.parse(v.date)),
              mes = dataObj.getMonth() + 1,
              data = dataObj.getDate() + "/" + mes + "/" + dataObj.getFullYear(),
              trecho = v.excerpt.rendered;

            return (
              <div className="pseudo_link" onClick={this.showVideo(v)}>
                <div className="posts_item_historico">
                  {v.meta ? 
                  <ReactPlayer
                    className="vid_destaque"
                    url={v.meta._video_url}
                  /> : null }
                  <div className="conteudo">
                    <span className="data">{data}</span>
                    <h4>{titulo}</h4>
                    <div className="trecho">{ReactHtmlParser(trecho)}</div>
                  </div>
                </div>
              </div>
            )
          }) : "Carregando..."}
          {this.state.posts.length ? 
          <div className="paginacao">
            <button onClick={this.voltarPagina}><PaginacaoVoltar /></button>
            {this.paginas().map(v => {
              let cssClass = v === this.state.pagina_atual ? "atual" : null
              return <button className={`${cssClass} __${this.context.orgao}`} onClick={this.paraPagina(v)}>{v}</button>
            })}
            <button onClick={this.avancarPagina}><PaginacaoAvancar /></button>
          </div> : null }
        </section>
      </LayoutSemBanner>
    ]
  }
}