import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import OrgaoContext from '../context'
import LayoutSemBanner from '../layout/sem_banner'
import MenuCaminho from '../components/menu_caminho'

import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';

export default class AdfDocsEspecificos extends Component {

  static contextType = OrgaoContext;

  constructor(props) {
    super(props)

    this.state = {
      grupo: null,
      anos_options: [],
      ano: null,
      carreiras_options: [],
      carreira: null,
      tipo: null,
      resultados: []
    }
  }

  tipos = [
    "Leis",
    "Decretos",
    "Instruções Normativas",
    "Portarias"
  ]

  handleChange = name => event => {
    if( name == "grupo" ){
      this.setState({ [name]: event.target.value }, () => this.checkGrupo() );
    }else{
      this.setState({ [name]: event.target.value });
    }
  };

  loadYears = () => {
    fetch(`${process.env.REACT_APP_WP_API}/wp-json/adf/v1/anos/docs_especificos`).then(
      (response) => {
        return response.json() 
      }).then(
        (r) => {
          this.setState({ anos_options : r }, () => console.log(this.state.anos_options));
    })
  }

  loadDocs = () => {
    console.log(`${process.env.REACT_APP_WP_API}/wp-json/adf/v1/docs_especificos?ano=${this.state.ano}&grupo=${this.state.grupo}&carreira=${this.state.carreira}&tipo=${this.state.ano}`)
    fetch(`${process.env.REACT_APP_WP_API}/wp-json/adf/v1/docs_especificos?ano=${this.state.ano || ""}&grupo=${this.state.grupo || ""}&carreira=${this.state.carreira || ""}&tipo=${this.state.tipo || ""}`).then(
      (response) => {
        return response.json() 
      }).then(
        (r) => {
          console.log(r)
          this.setState({ resultados : r }, () => console.log(this.state.resultados));
    })
  }

  grupos = [
    "Comunicação Social",
    "Fiscalização e Regulação",
    "Gestão Pública",
    "Técnico Específico",
    "Técnico Administrativo"
  ]

  checkGrupo = () => {

    console.log(this.state.grupo)
  
    let carreira_por_grupo = [
      [ "Jornalista" ],
      [ "Especialista em Meio Ambiente e Recursos Hídricos", "Técnico em Meio Ambiente e Recursos Hídricos" ],
      [ "Especialista em Políticas Públicas e Gestão Governamental" ],
      [ "Médico Veterinário" ],
      [ "Analista Técnico" ]
    ]

    this.grupos.forEach( (e, i) => {
      console.log(e, this.state.grupo)
      if(e == this.state.grupo){    
        this.setState({ carreiras_options : carreira_por_grupo[i] }, () => console.log(this.state.carreiras_options))
      }
    })
  }

  componentDidMount(){
    this.loadYears()
  }

  // Notar que os inputs do tipo select 
  render() {
    
    return (
      <LayoutSemBanner>
        <MenuCaminho />
        <h2>Legislação Específica</h2><br />
        <section id="docs_especificos">
          <div class="col_1">
          <FormControl variant="outlined" 
          classes={{ root: `input_container_root` }}
          style={{
            width : '100%'
          }}
          >
            <InputLabel
              ref={ref => { this.grupoRef = ReactDOM.findDOMNode(ref) }}
              htmlFor="grupo_input"
              classes={{ focused: `label_form __${this.context.orgao}` }}
            > Grupo Ocupacional </InputLabel>
            <Select
              native
              value={this.state.age}
              onChange={this.handleChange("grupo")}
              input={
                <OutlinedInput
                  id="grupo_input"
                  name="grupo"
                  classes={{ focused: `input_form_focused`, notchedOutline: `__${this.context.orgao}` }}
                  labelWidth={this.grupoRef ? this.grupoRef.offsetWidth : 0}
                />
              }
            >
              <option value=""></option>
              {this.grupos.map( e => (
                <option value={e}>{e}</option>
              ))}
            </Select>
          </FormControl>
          <FormControl 
            variant="outlined" 
            classes={{ root: `input_container_root` }}
            style={{
              width : '100%'
            }}>
            <InputLabel
              ref={ref => { this.carrRef = ReactDOM.findDOMNode(ref) }}
              htmlFor="carr_input"
              classes={{ focused: `label_form __${this.context.orgao}` }}
            > Carreira </InputLabel>
            <Select
              native
              value={this.state.age}
              onChange={this.handleChange("carreira")}
              input={
                <OutlinedInput
                  id="carr_input"
                  name="carreira"
                  classes={{ focused: `input_form_focused`, notchedOutline: `__${this.context.orgao}` }}
                  labelWidth={this.carrRef ? this.carrRef.offsetWidth : 0}
                />
              }
            >
              <option value=""></option>
              {this.state.carreiras_options.map( e => (
                <option value={e}>{e}</option>
              ))}
            </Select>
          </FormControl>
          <FormControl 
            variant="outlined" 
            classes={{ root: `input_container_root` }}
            style={{
              width : '60%',
              marginRight : '5%'
            }}>
            <InputLabel
              ref={ref => { this.tipoRef = ReactDOM.findDOMNode(ref) }}
              htmlFor="tipo_input"
              classes={{ focused: `label_form __${this.context.orgao}` }}
            > Tipo de Documento </InputLabel>
            <Select
              native
              value={this.state.age}
              onChange={this.handleChange("tipo")}
              input={
                <OutlinedInput
                  id="tipo_input"
                  name="tipo"
                  classes={{ focused: `input_form_focused`, notchedOutline: `__${this.context.orgao}` }}
                  labelWidth={this.tipoRef ? this.tipoRef.offsetWidth : 0}
                />
              }
            >
              <option value=""></option>
              {this.tipos.map( e => (
                <option value={e}>{e}</option>
              ))}
            </Select>
          </FormControl>
          <FormControl 
            variant="outlined" 
            classes={{ root: `input_container_root` }}
            style={{
              width : '30%'
            }}>
            <InputLabel
              ref={ref => { this.anoRef = ReactDOM.findDOMNode(ref) }}
              htmlFor="ano"
              classes={{ focused: `label_form __${this.context.orgao}` }}
            > Ano </InputLabel>
            <Select
              native
              value={this.state.age}
              onChange={this.handleChange("ano")}
              input={
                <OutlinedInput
                  id="ano"
                  name="ano"
                  classes={{ focused: `input_form_focused`, notchedOutline: `__${this.context.orgao}` }}
                  labelWidth={this.anoRef ? this.anoRef.offsetWidth : 0}
                />
              }
            >
              <option value=""></option>
              {this.state.anos_options.map( e => (
                <option value={e}>{e}</option>
              ))}
            </Select>
          </FormControl>
          <div className="alignright">
            <Button onClick={this.loadDocs} className={`btn __${this.context.orgao}`}>
              Consultar
            </Button>
          </div>
          </div>
          <div id="resultados" class="col2">
            <ul>
            {this.state.resultados.length ? this.state.resultados.map( e => (
              <li>
                <a 
                  href={`${e.base_url}/${e.src}`} 
                  target="_blank" >{e.src || "Arquivo sem link"}</a>
              </li>
            )) : "Não existem documentos a serem exibidos a partir filtros selecionados." }
            </ul>
          </div>
        </section>
      </LayoutSemBanner>
    )
  }
}