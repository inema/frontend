import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import OrgaoContext from '../context'
import LayoutSemBanner from '../layout/sem_banner'
import MenuCaminho from '../components/menu_caminho'
import ReactHtmlParser from 'react-html-parser';
import { Link } from "react-router-dom"
import { PaginacaoVoltar, PaginacaoAvancar, SmallArrow } from "../components/own_icons"

import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';

import ImageGallery from 'react-image-gallery';
import "react-image-gallery/styles/css/image-gallery.css";
import Modal from '@material-ui/core/Modal';

/**
 * Diantemão já aviso que esse é o código mais bagunçado, muitas modificações em cima da hora por causa do cliente que não compreende a forma de trabalho, infelizmente não tem como fazer um bom código dessa forma devido a demanda e tempo hábil!
 */

export default class AdfDocumentos extends Component {

  static contextType = OrgaoContext;

  constructor(props) {
    super(props)
    
    const date_obj = new Date()

    this.state = {
      ano: this.props.routeProps.match.params.ano || date_obj.getFullYear(),
      anos: [],
      activeTab: "calendario",
      items: [],
      listMonths: {},
      routeProps: null,
      galeria: [
        { 
          original: null,
          thumbnail: null
        }
      ],
      modalIsOpened: false
    }
  }

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });

    if(name === "ano"){
      let value = event.target.value;

      // Caso o select modificado seja o de ano alterar a url
      if(this.state.routeProps){
        console.log(this.props.routeProps.match.url, this.state.routeProps.match.url)

        // Perceber que uma função semelhante a essas linhas de código abaixo tem no arquivo App.js, possivelmente modularizar a função posteriormente
        let newRouteProps = this.state.routeProps;
        console.log(newRouteProps)
        newRouteProps.match.url = newRouteProps.match.url.replace(newRouteProps.match.params.ano, value);
        newRouteProps.match.url = newRouteProps.match.url.replace(newRouteProps.match.params.aba, this.state.activeTab);
        // Atualiza orgao
        newRouteProps.match.params.ano = value;
        // Encaminha para nova url
        console.log(newRouteProps)
        this.setState({ routeProps : newRouteProps }, () => {
          this.state.routeProps.history.push(this.state.routeProps.match.url)
          this.loadItems()
        })
      }
    }
  };

  loadItems = () => {
    console.log("load items")
    // console.log(this.state.ano)    
    // console.log(this.endpoints[this.state.activeTab])    
    // console.log(`${process.env.REACT_APP_WP_API}/wp-json/adf/v1/${this.endpoints[this.state.activeTab]}`);
    console.log(`${process.env.REACT_APP_WP_API}/wp-json/adf/v1/${this.endpoints[this.state.activeTab]}/${this.state.ano ? this.state.ano : null }`)
    fetch(`${process.env.REACT_APP_WP_API}/wp-json/adf/v1/${this.endpoints[this.state.activeTab]}/${this.state.ano ? this.state.ano : null }`).then(
      (response) => {
        console.log("first promise")
        return response.json() 
      }).then(
        (r) => {
          this.setState({ items : r }, () => { 
            // Hack para comunicados que foi o pedido de última hora maior
            if(this.state.activeTab !== "comunicados" ) this.orderItems()
            console.log("$$", this.state.items)
          });
    })
  }

  loadYears = () => {
    fetch(`${process.env.REACT_APP_WP_API}/wp-json/adf/v1/anos/${this.state.activeTab}`).then(
      (response) => {
        return response.json() 
      }).then(
        (r) => {
          this.setState({ anos : r });
    })
  }

  componentDidMount(){
    this.endpoints = {
      "calendario" : `agenda`,
      "convocatorias" : `convocatorias`,
      "comunicados" : `comunicados`,
      "atas" : `atas`,
      "manuais" : `manuais`,
      "relatorios_anuais" : `relatorios`
    }

    // Atualiza dados pela url
    let params = this.props.routeProps.match.params

    // Caso não tenha o parâmetro de anos coloca o ano atual
    if(typeof(params.ano) == "undefined"){
      console.log("sem ano")
      let data_atual = new Date();
      params.ano = data_atual.getFullYear()
    }

    this.setState({
      ano: params.ano,
      activeTab: params.aba,
      routeProps: this.props.routeProps
    }, this.loadThings())

  }
  
  loadThings = () => {
    console.log("INICIOLOADTHINGS", this.props.routeProps.match.params.aba )
    this.setState({
      activeTab: this.props.routeProps.match.params.aba 
    }, () => {
      this.loadItems()
      this.loadYears()
    })
  }

  componentDidUpdate(){
    // Verifica se a aba atual é a mesma da aba que estava
    console.log(this.props.routeProps.match.params.aba, this.state.activeTab)
    if(this.props.routeProps.match.params.aba !== this.state.activeTab){
      this.loadThings()
    }
  }

  orderItems = () => {
    let listMonths = this.state.listMonths

    this.state.items.forEach( ( e, i, a ) => {

    if( i === 0 ) listMonths = {}

    let m = [
      "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"
    ]

    // Pega objeto de data para facilitar captura de data e hora do evento. Calendário funciona com datetime e atas apenas com referência de data conforme requisito e especificações de design
    const date_obj = new Date(e.datetime || e.data)
    const last_date_obj = a[i-1] ? new Date(a[i-1].datetime) : null

    // Compara com a data do último evento para ver se trocou de mês
    const changedMonth = !last_date_obj || m[date_obj.getMonth()] != m[last_date_obj.getMonth()] ? true : false

    if(changedMonth){
      listMonths[m[date_obj.getMonth()]] = []
      a.forEach( el => {
        const child_date_obj = new Date(el.datetime || el.data)
        if(date_obj.getMonth() === child_date_obj.getMonth()){
          listMonths[m[date_obj.getMonth()]].push( el )
        }
      })
    }

    })

    console.log(listMonths)

    this.setState({ listMonths : listMonths }, () => console.log(this.state))
  }

  reg_fotograficos = imagens => {
    // Verifica se as imagens de tal ata retorna null
    if(imagens.length){
      return <button className={`a_like cor_orgao __${this.context.orgao}`} onClick={this.loadGaleria(imagens)}>Registros fotográficos</button>
    }
  }

  loadGaleria = galeria => () => {
    this.setState({ 
      modalIsOpened: true,
      galeria
    });
  }

  handleClose = () => {
    this.setState({ modalIsOpened: false });
  }

  render() {
    const activeTab = this.state.activeTab;
    const ano = this.state.ano;
    const context = this.context;

    // if(this.props.routeProps.match.params.aba !== this.state.activeTab){
    //   console.log("DEVE ALTERAR 2")
    //   this.setState({ activeTab: this.props.routeProps.match.params.aba })
    // }

    // console.log("$$", Object.entries(this.state.items))

    return (
      <LayoutSemBanner>
        <MenuCaminho/>
        <h2>Documentos</h2><br/>
        <FormControl variant="outlined" classes={{ root: `input_container_root` }}>
          <InputLabel
            ref={ref => { this.anoRef = ReactDOM.findDOMNode(ref) }}
            htmlFor="ano_input"
            classes={{ focused: `label_form __${this.context.orgao}` }}
          > Ano </InputLabel>
          <Select
            native
            value={this.state.ano}
            onChange={this.handleChange("ano")}
            input={
              <OutlinedInput
                id="ano_input"
                name="ano"
                classes={{ focused: `input_form_focused`, notchedOutline: `__${this.context.orgao}` }}
                labelWidth={this.anoRef ? this.anoRef.offsetWidth : 0}
              />
            }
          >
            {this.state.anos.map( e => {
              let selected = null
              if(e == this.state.ano) selected = "selected"
              return <option value={e} selected={selected}>{e}</option> 
            })}
          </Select>
        </FormControl>
        <section id="abas">
          <ul>
            <li class={` ${activeTab == "calendario" ? "active" : ""} __${this.context.orgao}`}><Link to={`/${context.orgao}/adf/calendario/${ano}`}>Calendário</Link></li>
            <li class={` ${activeTab == "convocatorias" ? "active" : ""} __${this.context.orgao}`}><Link to={`/${context.orgao}/adf/convocatorias/${ano}`}>Convocatórias</Link></li>
            <li class={` ${activeTab == "atas" ? "active" : ""} __${this.context.orgao}`}><Link to={`/${context.orgao}/adf/atas/${ano}`}>Atas</Link></li>
            <li class={` ${activeTab == "manuais" ? "active" : ""} __${this.context.orgao}`}><Link to={`/${context.orgao}/adf/manuais/${ano}`}>Manuais</Link></li>
            <li class={` ${activeTab == "relatorios_anuais" ? "active" : ""} __${this.context.orgao}`}><Link to={`/${context.orgao}/adf/relatorios_anuais/${ano}`}>Relatórios</Link></li>
            <li class={` ${activeTab == "comunicados" ? "active" : ""} __${this.context.orgao}`}><Link to={`/${context.orgao}/adf/comunicados/${ano}`}>Comunicados</Link></li>
          </ul>
        </section>
        <Modal
            id="modal_galeria" 
            open={this.state.modalIsOpened}
            onClose={this.handleClose}
          >
          <ImageGallery items={this.state.galeria} />
        </Modal>
        {activeTab == "calendario" || activeTab == "atas" ?
        <section id="adf_calendario">
          {Object.entries(this.state.listMonths).map( el => (
              <ExpansionPanel
              classes={{
                root: `accord_parent`
              }}>
                <ExpansionPanelSummary
                  expandIcon={<SmallArrow/>}
                  aria-controls="panel1a-content"
                  id="panel1a-header"
                  classes={{
                    root: `__${this.context.orgao}`
                  }}
                >
                  <Typography>{el[0]}</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails
                classes={{
                  root: `compromissos_box`
                }}>
                {el[1].map( elem => {

                  if(activeTab === "calendario"){
                    // console.log("xxoiii")
                    let data = new Date(elem.datetime)
                    return ( 
                      <>                 
                      <h3>{data.getDate()}/{data.getMonth()+1} - {data.getHours()}h</h3>
                      <h5>{elem.local}</h5>
                      <div>{ReactHtmlParser(elem.descricao)}</div>
                      </>
                    )}else{
                    console.log("oieee")
                    let nome_arquivo = elem.src ? elem.src.split("/").pop() : "Nome provisório"
                    let data = new Date(elem.data)

                    return (
                      <>
                      <h3>{data.getDate()}/{data.getMonth()+1}</h3>
                      <div><a href={elem.src} target="_blank" style={{ display : "block" }}>{elem.titulo} - {nome_arquivo}</a>
                      {elem.imagens ? this.reg_fotograficos(elem.imagens) : null}
                      </div>
                      </>
                    )
                  }
                })}
                </ExpansionPanelDetails>
              </ExpansionPanel>
          ))}
        </section> : activeTab === "comunicados" ?
        <section>
          {Object.entries(this.state.items).length > 0 ? 
          Object.entries(this.state.items).map( el => {
            let data = new Date(el[0])
            return (
              <ExpansionPanel
              classes={{
                root: `accord_parent`
              }}>
                <ExpansionPanelSummary
                  expandIcon={<SmallArrow/>}
                  aria-controls="panel1a-content"
                  id="panel1a-header"
                  classes={{
                    root: `__${this.context.orgao}`
                  }}
                >
                  <Typography>{el[1][0] ? `${data.getDate()}/${data.getMonth()+1} - ${el[1][0].assunto}` : null}</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails
                classes={{
                  root: `compromissos_box`
                }}>
                <p>{el[1][0] ? el[1][0].descricao : null}</p>
                {Array.isArray(el[1]) ? el[1].map( elem => {
                  return (
                    <a href={elem.src} target="_blank" >{elem.titulo}</a>
                  )
                }) : null}
                </ExpansionPanelDetails>
              </ExpansionPanel>
            )
          }): "Não foi encontrado nenhum comunicado referente a esse ano." }
        </section> : 
      <section>
        {Array.isArray(this.state.items) ? this.state.items.map( (e, i, a) => {
          let nome_arquivo = e.src ? e.src.split("/").pop() : "Nome temporário"
          return (
            <a href={e.src} target="_blank" style={{ display : "block" }}>{nome_arquivo}</a>
          )
        }) : 'Carregando...'}
      </section> }
      </LayoutSemBanner>
    )
  }
}