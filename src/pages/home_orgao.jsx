import React, { Component } from 'react'
import PageWrap from '../layout/page_wrap'
import PainelTempo from '../components/widget/painel_tempo'
import OrgaoContext from '../context'

import WidgetNoticias from '../components/widget/noticias';
import WidgetDestaque from '../components/widget/destaque';
import WidgetAniversariantes from '../components/widget/aniversariantes';
import WidgetCalendario from '../components/widget/calendario';
import WidgetAgendaCultural from '../components/widget/agenda_cultural';
import WidgetVideos from '../components/widget/videos';
import WidgetNovosColaboradores from '../components/widget/novos_colaboradores';
import WidgetBanners from '../components/widget/banners';
import WidgetFicaDica from '../components/widget/fica_dica';
import WidgetAltosPapos from '../components/widget/altos_papos';

export default class Home extends Component {

  static contextType = OrgaoContext;

  constructor(props) {
    super(props)

    this.state = {
      /**
       * As chaves são as mesmas do backend com 
       * um Componente referenciado de execução
       */
      loadedComponents: {
        noticias: WidgetNoticias,
        destaque: WidgetDestaque,
        painel_ambiental: PainelTempo,
        aniversariantes: WidgetAniversariantes,
        calendario: WidgetCalendario,
        agenda_cultural: WidgetAgendaCultural,
        videos: WidgetVideos,
        novos_colaboradores: WidgetNovosColaboradores,
        banners: WidgetBanners,
        fica_dica: WidgetFicaDica,
        altos_papos: WidgetAltosPapos
      },
      widgets: null
    }
  }

  loadWidgets = () => {
    fetch(`${process.env.REACT_APP_WP_API}/wp-json/intranet_config/v1/tela_inicial/`).then(
      (response) => response.json() ).then(
        (r) => {
          this.setState({ widgets : r }, console.log(this.state.widgets));
      })
  }

  componentDidMount(){
    console.log(this.props);

    if(this.context.routeProps){

      if(this.context.routeProps.location !== this.props.location){
        this.context.changeRouteProps(this.props);
      }

      // Verifica órgão passado pela URL e atribui ao orgao do context caso seja diferente
      if(this.props.match.params.orgao !== this.context.orgao){
        this.context.alterarOrgao();
      }

    }else{
      this.context.changeRouteProps(this.props);
    }

    this.loadWidgets();
  }

  render() {

    const widgets = this.state.widgets;
    const loadedComponents = this.state.loadedComponents;

    return (
      <PageWrap>
        <OrgaoContext.Consumer>
          {context => 
              <main className="index">
                {/* <WidgetNoticias/>
                <WidgetDestaque/>
                <PainelTempo/>
                <WidgetFicaDica/>
                <WidgetAniversariantes/>
                <WidgetCalendario/>
                <WidgetAgendaCultural/>
                <WidgetAltosPapos/>
                <WidgetVideos/>
                <WidgetNovosColaboradores/>
                <WidgetBanners/> */}
                {widgets ? widgets.map( e => {
                  const C = loadedComponents[e.slug]
                  return C ? <C order={e.order} /> : null
                  // return( <div>{e.titulo}</div>)
                }) : "Carregando..."}
            </main>
          }
        </OrgaoContext.Consumer>
      </PageWrap>
    )
  }
}