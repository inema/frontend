import React, { Component } from 'react'
import LayoutSemBanner from '../layout/sem_banner'
import ReactHtmlParser from 'react-html-parser';
import changeUrl from "../functions/change_url"
import MenuCaminho from '../components/menu_caminho'
import ImagemDestaque from '../components/imagem_destaque'
import OrgaoContext from '../context'
import GaleriaRodape from '../components/galeria_rodape'

export default class StaticRoute extends Component {

  static contextType = OrgaoContext;

  state = {
    title: this.props.data.title.rendered,
    content: "Carregando...",
    shouldUpdate: true,
    footerGallery: null
  }

  componentDidMount(){
    if(this.props.data.meta){
      let meta = this.props.data.meta
      console.log(meta._ft_gall)
      if(meta._ft_gall !==  ""){
        this.setState({ footerGallery: JSON.parse(meta._ft_gall_imgs) }, () => console.log(this.state.footerGallery))
      }
    }
    changeUrl(this.props.data.content.rendered, `/${this.context.orgao}`)
    .then( data => this.setState({ content: ReactHtmlParser(data) }) )
  }

  is404 = () => {
    if(this.state.shouldUpdate){
      this.setState({
        shouldUpdate: false,
        title: "Erro 404",
        content: ReactHtmlParser("Página não encontrada em nosso banco de dados. <br/>Por favor verifique a digitação do caminho na url e tente novamente.")
      })
    }
  }

  // recebe props.data com todas as informações da API WP
  render(){
    return (
      <LayoutSemBanner>
        <MenuCaminho is404={this.is404} />
        <h2>{this.state.title}</h2>
        <ImagemDestaque/>
        <section className="conteudo">
          {this.state.content}
        </section>
        {this.state.footerGallery ? 
        <GaleriaRodape items={this.state.footerGallery}/> : ""}
      </LayoutSemBanner>
    )
  }
}