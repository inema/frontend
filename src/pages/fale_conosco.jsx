import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import OrgaoContext from '../context'
import LayoutSemBanner from '../layout/sem_banner'
import MenuCaminho from '../components/menu_caminho'
import TextField from '@material-ui/core/TextField'

import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';

import { SocialIconFacebook, SocialIconInstagram, SocialIconFlickr, SocialIconTwitter } from '../components/own_icons'

export default class FaleConosco extends Component {

  static contextType = OrgaoContext;
  
  constructor(props){
    super(props);

    this.state = {
      motivo: null,
      nome: null,
      email: null,
      ramal: null,
      mensagem: null,
      feedback: ""
    }
  }
  
  componentDidMount() {
    const context = this.context;
    if(context.routeProps.location !== this.props.routeProps.location){
      context.changeRouteProps(this.props.routeProps)
    }
  }

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  };

  enviarFormulario = () => {

    this.setState({ feedback: "Carregando..." })

    let formData = new FormData()
    formData.append("motivo", this.state.motivo)
    formData.append("nome", this.state.nome)
    formData.append("email", this.state.email)
    formData.append("ramal", this.state.ramal)
    formData.append("mensagem", this.state.mensagem)

    fetch(`${process.env.REACT_APP_WP_API}/wp-json/smtp_api/v1/send`, {
      method: "POST",
      body: formData
    }).then(
      (response) => {
        return response.json() 
      }).then(
        r => {
          // console.log("$$$", "RESPOSTA DE EMAIL", r)
          this.setState({ feedback: r.status })
    })
  }
  
  render(){

    const motivos = [
      "Sugestões",
      "Dúvidas",
      "Erro no Site",
      "Elogios",
      "Críticas",
      "Outros"
    ]

    return (
      <LayoutSemBanner>
        <MenuCaminho/>
        <h2>Fale Conosco</h2>
          <section className="row" id="sec_fale_conosco">
            <div className="col-6">
              <hr/>
              <FormControl variant="outlined" classes={{ root : `input_container_root` }}>
                <InputLabel
                  ref={ref => { this.motivoRef = ReactDOM.findDOMNode(ref) }}
                  htmlFor="motivo_input"
                  classes={{ focused: `label_form __${this.context.orgao}`}}
                > Motivo * </InputLabel>
                <Select
                  native
                  value={this.state.age}
                  onChange={this.handleChange("motivo")}
                  input={
                    <OutlinedInput
                      id="motivo_input"
                      name="motivo"
                      classes={{ focused: `input_form_focused`, notchedOutline : `__${this.context.orgao}`}}
                      labelWidth={this.motivoRef ? this.motivoRef.offsetWidth : 0}         
                    />
                  }
                >
                  <option value="" />
                  {motivos.map( mot => <option value={mot}>{mot}</option> )}
                </Select>
              </FormControl>

              <FormControl variant="outlined" classes={{ root : `input_container_root` }}>
                <InputLabel
                  ref={ref => { this.nomeRef = ReactDOM.findDOMNode(ref); }}
                  htmlFor="nome_input"
                  classes={{ focused: `label_form __${this.context.orgao}`}}
                > Nome </InputLabel>
                <OutlinedInput
                  id="nome_input"
                  name="nome"
                  value={this.state.nome}
                  classes={{ focused: `input_form_focused`, notchedOutline : `__${this.context.orgao}`}}
                  onChange={this.handleChange("nome")}
                  labelWidth={this.nomeRef ? this.nomeRef.offsetWidth : 0}
                />
              </FormControl>

              <FormControl variant="outlined" classes={{ root : `input_container_root` }}>
                <InputLabel
                  ref={ref => { this.labelRef = ReactDOM.findDOMNode(ref); }}
                  htmlFor="email_input"
                  classes={{ focused: `label_form __${this.context.orgao}`}}
                > Email </InputLabel>
                <OutlinedInput
                  id="email_input"
                  name="email"
                  type="email"
                  value={this.state.name}
                  classes={{ focused: `input_form_focused`, notchedOutline : `__${this.context.orgao}`}}
                  onChange={this.handleChange("email")}
                  labelWidth={this.labelRef ? this.labelRef.offsetWidth : 0}
                />
              </FormControl>

              <FormControl variant="outlined" classes={{ root : `input_container_root` }}>
                <InputLabel
                  ref={ref => { this.ramalRef = ReactDOM.findDOMNode(ref); }}
                  htmlFor="ramal_input"
                  classes={{ focused: `label_form __${this.context.orgao}`}}
                >
                  Ramal
                </InputLabel>
                <OutlinedInput
                  id="ramal_input"
                  name="ramal"
                  value={this.state.ramal}
                  classes={{ focused: `input_form_focused`, notchedOutline : `__${this.context.orgao}`}}
                  type="number"
                  onChange={this.handleChange("ramal")}
                  labelWidth={this.ramalRef ? this.ramalRef.offsetWidth : 0}
                />
              </FormControl>

              <FormControl variant="outlined" classes={{ root : `input_container_root  textarea` }}>
                <InputLabel
                  ref={ref => { this.mensagemRef = ReactDOM.findDOMNode(ref); }}
                  htmlFor="mensagem_input"
                  classes={{ focused: `label_form __${this.context.orgao}`}}

                > Mensagem * </InputLabel>
                <OutlinedInput
                  id="mensagem_input"
                  name="mensagem"
                  value={this.state.name}
                  classes={{ focused: `input_form_focused`, notchedOutline : `__${this.context.orgao}`}}
                  onChange={this.handleChange("mensagem")}
                  labelWidth={this.mensagemRef ? this.mensagemRef.offsetWidth : 0}
                  multiline={true}
                />
              </FormControl>
              
              <div className="alignright">
                <div>{this.state.feedback}</div>
                <Button className={`btn __${this.context.orgao}`} onClick={this.enviarFormulario}>Enviar</Button>
              </div>


            </div>
            <div className="col-6">
              <h4>Telefones</h4>
              <hr/>

              <span className={`info_grande __${this.context.orgao}`}>3118-6089 / 3118-3812</span>
              <span>Dúvidas/Sugestões</span>
              <hr/>

              <span className={`info_grande __${this.context.orgao}`}>0800 284 1400 | 3118-4553 | 3118-4554</span>
              <span>Ouvidoria INEMA</span>
              <hr/>

              <span className={`info_grande __${this.context.orgao}`}>0800 284 0011</span>
              <span>Ouvidoria Geral do Estado</span>
              <hr/>

              <a href={this.context.orgao === "sema" ? "https://www.facebook.com/semabahia" : "#" } target="_blank" ><SocialIconFacebook context={this.context}/></a>
              <a href={this.context.orgao === "sema" ? "https://www.instagram.com/semabahia" : "#" } target="_blank" ><SocialIconInstagram context={this.context}/></a>
              <a href={this.context.orgao === "sema" ? "https://twitter.com/Sema_Bahia" : "#" } target="_blank" ><SocialIconTwitter context={this.context}/></a>
              <a href={this.context.orgao === "sema" ? "https://www.flickr.com/photos/semabahia" : "#" } target="_blank" ><SocialIconFlickr context={this.context}/></a>

            </div>
          </section>
      </LayoutSemBanner>
    )
  }  
}