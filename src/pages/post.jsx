import React, { Component } from 'react'
import OrgaoContext from '../context'
import LayoutSemBanner from '../layout/sem_banner'
import MenuCaminho from '../components/menu_caminho'
import ReactHtmlParser from 'react-html-parser';
import { Link } from "react-router-dom"
import { PaginacaoVoltar, PaginacaoAvancar } from "../components/own_icons"
import GaleriaRodape from '../components/galeria_rodape'

export default class Posts extends Component {

  static contextType = OrgaoContext;

  constructor(props){
    super(props)

    this.state = {
      post: {
        title: {
          rendered: "Carregando..."
        },
        content: {
          rendered: "Carregando..."
        },
      }, 
      posts: [],
      paginas: 1,
      pagina_atual: 1,
      cpt: "posts", // nome do custom post type que está no momento
      footerGallery: null
    }
  }

  // Dicionário do custom post type e o título a ser mostrado
  titles = {
    "posts" : "Notícias",
    "altos-papos" : "Altos Papos",
    "ver-de-dentro" : "Ver de Dentro",
    "vem-ai" : "Vem Aí"
  }

  //  hack para usar o post padrão com a chamada de noticias via api
  slugs = {
    "posts" : "noticias"
  }

  /* ----------
    Função para carregar os posts
  ---------- */
  loadPosts = () => {
    console.log(`${process.env.REACT_APP_WP_API}/wp-json/wp/v2/${this.state.cpt}?per_page=5&page=${this.state.pagina_atual}`);
    fetch(`${process.env.REACT_APP_WP_API}/wp-json/wp/v2/${this.state.cpt}?per_page=5&page=${this.state.pagina_atual}`).then(
      (response) => {
        // console.log(response.headers.get("X-WP-Total"))
        this.setState({ paginas: parseInt(response.headers.get("X-WP-TotalPages")) });
        return response.json() 
      } ).then(
        (r) => {
          console.log(r);
          this.setState({ posts : r });
          console.log(this.state);
        })
  }

  componentDidMount(){
    const props = this.props;
    const context = this.context;
    
    if(context.routeProps.location !== props.routeProps.location){
      context.changeRouteProps(props.routeProps)
    }

    const slug = props.routeProps.match.params.slug;
    
    if(slug){
    fetch(`${process.env.REACT_APP_WP_API}/wp-json/wp/v2/${props.cpt}?slug=${slug}`).then(
      (response) => response.json() ).then(
        (r) => {
          this.setState({ post : r[0] }, () => {
            if(this.state.post.meta){
              let meta = this.state.post.meta
              console.log(meta._ft_gall)
              if(meta._ft_gall !==  ""){
                this.setState({ footerGallery: JSON.parse(meta._ft_gall_imgs) }, () => console.log(this.state.footerGallery))
              }
            }
          });
        })
    }

    this.setState({ cpt: props.cpt }, () => this.loadPosts() )

  }

  componentDidUpdate(prevProps, prevState){
    if(prevState.pagina_atual !== this.state.pagina_atual) this.loadPosts();
  }

  paginas = () => {
    const paginas = [];
    // O objetivo é mostrar apenas cinco páginas, duas antes e duas depois da atual caso não sejam as primeiras nem as últimas
    for (let i = 1; i <= this.state.paginas; i++) {
      paginas.push(i)            
    }
    return paginas;
  }

  voltarPagina = () => {
    // Verifica se é a primeira página
    if(this.state.pagina_atual === 1){
      return true
    }else{
      this.setState({ pagina_atual : this.state.pagina_atual-1 })
    }
  }

  avancarPagina = () => {
    // Verifica se é a última página
    if(this.state.pagina_atual === this.state.paginas){
      return true
    }else{
      this.setState({ pagina_atual : this.state.pagina_atual+1 }, () => console.log("pagina atual: ", this.state.pagina_atual))
    }
  }

  paraPagina = pag => e => {
    this.setState({ pagina_atual : pag }, () => console.log("pagina atual: ", this.state.pagina_atual))
  }

  render(){

    const imgUrl = this.state.post.better_featured_image ? this.state.post.better_featured_image.source_url : "";

    if(this.props.routeProps.match.params.slug){

      const meses = [
        "", "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"
      ]

      const data = new Date(this.state.post.date)
      const dataHumanReadable = `${data.getDate()} de ${meses[data.getMonth()+1]} de ${data.getFullYear()}` 

      
      // Traduzindo data e hora do evento para ser legível o usuário leigo
      let VAdataHumanReadable = '';
      if(this.state.cpt === "vem-ai"){
        if(this.state.post.meta){
          const VAdata = new Date(this.state.post.meta._data_hora)
          VAdataHumanReadable = `${VAdata.getDate()}/${VAdata.getMonth()+1} - ${VAdata.getHours()}:${VAdata.getMinutes()}` 
        }
      }

      // Mostra notícia específica
      return (
        <LayoutSemBanner 
          full={this.props.sidebar} 
          aside={this.props.sidebar} 
          conteudo={this.state.posts}
        >
          <MenuCaminho post />
          {/* O padrão de postagem do Altos Papos é diferente, por isso a condicional, talvez futuramente seja melhor dividir os tipos de postagem por componentes @Carlos Júnior dificultando a vida da galera...kkkk  */}
          {this.state.cpt === "altos-papos" ? ( 
          <>
          <h2>{ReactHtmlParser(this.state.post.title.rendered)}</h2>
          <span className="data_postagem">{dataHumanReadable}</span>
          <section className={`cpt_${this.state.cpt}`}>
            {imgUrl ? <div className="img_destaque" style={{ backgroundImage: `url("${imgUrl}")` }}/> : null}
              <h5>Altos papos com</h5>
              <h3>{this.state.post.meta ? this.state.post.meta._nome_entrevistado : null}</h3>
              {ReactHtmlParser(this.state.post.content.rendered)}
          </section> 
          </> ) : (
            <> 
          <h2>{ReactHtmlParser(this.state.post.title.rendered)}</h2>
          <span className="data_postagem">{dataHumanReadable}</span>
          <section className={`cpt_${this.state.cpt}`}>
            {imgUrl ? <div className="img_destaque" style={{ backgroundImage: `url("${imgUrl}")` }}/> : null}
              <section className={this.state.cpt !== 'posts' ? 'conteudo' : ''}>
                {/* No Vem Aí tem que colocar a data e  */}
                {this.state.cpt === "vem-ai" ? ( <div>
                  <strong>{VAdataHumanReadable}</strong>
                  <div>{this.state.post.meta ? this.state.post.meta._local : null}</div><br/>
                </div> ) : null}
                {/* Todos os outros tem apenas */}
                {ReactHtmlParser(this.state.post.content.rendered)}
              </section>
          </section>
          </> )}
          {this.state.footerGallery ? 
          <GaleriaRodape items={this.state.footerGallery}/> : ""}
        </LayoutSemBanner>
      )
    }else{
      // Mostra histórico de Notícias
      return (
        <LayoutSemBanner>
          <MenuCaminho/>
          <h2>{this.titles[this.state.cpt]}</h2>
            <section id="historico_posts" className="conteudo">
              {this.state.posts.length ? this.state.posts.map( v => {
                let titulo = v.title.rendered,
                dataObj = new Date(Date.parse(v.date)),
                mes = dataObj.getMonth()+1,
                data = dataObj.getDate()+"/"+mes+"/"+dataObj.getFullYear(),
                trecho = v.excerpt.rendered;

                return (
                  <Link to={`${this.slugs[this.state.cpt] ? this.slugs[this.state.cpt] : this.state.cpt}/${v.slug}`} onClick={this.forceUpdate}>
                  <div className="posts_item_historico">
                    {v.better_featured_image ? <div className="img_destaque" style={{ backgroundImage: `url("${v.better_featured_image ?v.better_featured_image.media_details.sizes.thumbnail ? v.better_featured_image.media_details.sizes.thumbnail.source_url : imgUrl : ''}")` }}/> : null}
                    <div className="conteudo">
                      <span className="data">{data}</span>
                      <h4>{titulo}</h4>
                      {this.state.cpt === 'altos-papos' ? 
                      <div className="trecho">{ReactHtmlParser(v.meta._nome_entrevistado)}</div> :
                      <div className="trecho">{ReactHtmlParser(trecho)}</div>}
                    </div>
                  </div>
                  </Link>
                )
              }) : "Carregando..."}
              <div className="paginacao">
                <button onClick={this.voltarPagina}><PaginacaoVoltar/></button>
                {this.paginas().map( v => {
                  let cssClass = v === this.state.pagina_atual ? "atual" : null
                  return <button className={`${cssClass} __${this.context.orgao}`} onClick={this.paraPagina(v)}>{v}</button>
                })}
                <button onClick={this.avancarPagina}><PaginacaoAvancar/></button>
              </div>
            </section>
        </LayoutSemBanner>
      )
    }
  }
}