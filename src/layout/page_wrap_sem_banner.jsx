import React, { Component } from 'react'
import HeaderMain from '../layout/header_main'
import Banner from '../components/banner'
import FooterMain from '../layout/footer_main'
import OrgaoContext from '../context'

export default class PageWrap extends Component {
  constructor(props){
    super(props)
  }

  render() {
    return (      
        <OrgaoContext.Consumer>
          {context =>
            <>
              <HeaderMain context={context} />
              <div className="container">
              <Banner context={context} nobanner/>
                {this.props.children}
              <FooterMain context={context} />
              </div>
            </>}
        </OrgaoContext.Consumer>
    )
  }
}