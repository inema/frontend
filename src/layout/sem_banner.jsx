import React, { useContext } from 'react'
import OrgaoContext from '../context'
import PageWrapSemBanner from './page_wrap_sem_banner'
import AsideNoticias from '../components/aside_noticias'

export default props => {

  const context = useContext(OrgaoContext);
  const conteudo = props.conteudo;

  return (
    <PageWrapSemBanner>
      <section className={`institucional_conteudo conteudo_pagina ${props.full ? "full" : null} flex`}>
        <main className={`post __${context.orgao} ${props.aside ? "aside" : null}`}>
          {props.children}
        </main>
        {props.aside ? <AsideNoticias conteudo={conteudo} /> : null}
      </section>
    </PageWrapSemBanner>
  )
}