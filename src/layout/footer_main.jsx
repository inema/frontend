import React, { Component } from 'react'
import LinkOrgao from '../components/link_orgao'
import { SocialIconFacebook, SocialIconFlickr, SocialIconInstagram, SocialIconTwitter } from '../components/own_icons'

export default class FooterMain extends Component {
  constructor(props){
    super(props);
  }
  
  render() {

    const context = this.props.context;

    return (
      <>
        <footer className="main">
          <div className="flex">
            <div className="box_contato">{
              context.orgao === "inema" ? (
                <>
                <img alt="Logo Inema" src="/img/inema_flat_white_logo.png"/>
                <span>Avenida Luís Viana Filho, 6ª Avenida, nº 600 - CAB - CEP 41.745-900 - Salvador/BA <br/> </span><span> (71) 3118 4267 | 4500 | 4555</span>
                </>
              ) : (
                <>
                <img alt="Logo Governo da Bahia" src="/img/gov_bahia_flat_white_logo.png"/>
                <span>Avenida Luís Viana Filho, 6ª Avenida, nº 600 - CAB - CEP 41.745-900 - Salvador/BA <br/> </span><span> (71) 3118 4267 | 4500 | 4555</span>
                </>
              )
            }</div>
            <div className="links_referencia">
            <ul>
              <li className="titulo">INSTITUCIONAL</li>
              <li><LinkOrgao to="historico">Histórico</LinkOrgao></li>
              <li><LinkOrgao to="titulares">Titulares</LinkOrgao></li>
              <li><LinkOrgao to="unidades">Unidades</LinkOrgao></li>
              {context.orgao === "sema" ? <li><LinkOrgao to="orgaos-colegiados">Órgãos colegiados</LinkOrgao></li> : null}
              <li><LinkOrgao to="nosso-papel">Nosso papel</LinkOrgao></li>
              <li><LinkOrgao to="marcas-manuais">Marcas e manuais</LinkOrgao></li>
            </ul>
            <ul>
              <li className="titulo">SERVIÇOS</li>
              <li><a href="http://www.portaldoservidor.ba.gov.br/servicos/folha-de-pagamento/contracheque" target="_blank">Contracheque</a></li>
              <li><a href="https://www.planserv.ba.gov.br/" target="_blank">Planserv</a></li>
              <li><a href="http://www.portaldoservidor.ba.gov.br/balcao-previdenciario" target="_blank">Aposentadoria</a></li>
              <li><a href="http://www.portaldoservidor.ba.gov.br/clube-de-desconto" target="_blank">Clube de desconto</a></li>
            </ul>
            <div className="redes_sociais">
              <ul>
                <li><a href={context.orgao === "sema" ? "https://www.facebook.com/semabahia" : "#" } target="_blank" ><SocialIconFacebook context={context} noColor/></a></li>
                <li><a href={context.orgao === "sema" ? "https://www.instagram.com/semabahia" : "#" } target="_blank" ><SocialIconInstagram context={context} noColor/></a></li>
                <li><a href={context.orgao === "sema" ? "https://twitter.com/Sema_Bahia" : "#" } target="_blank" ><SocialIconTwitter context={context} noColor/></a></li>
                <li><a href={context.orgao === "sema" ? "https://www.flickr.com/photos/semabahia" : "#" } target="_blank" ><SocialIconFlickr context={context} noColor/></a></li>
              </ul>
              <ul>
                <li>Contato</li>
                <li>Ouvidoria</li>
              </ul>
            </div>
            </div>
            {/* <div className="instagram">
            <img alt="Instagram incompleto" src="/img/instagram_fill.png" />
            </div> */}
          </div>
          <div className="creditos">
            <img src="/img/logo_inema.jpg"/>
            <img src="/img/logo_governo_bahia.jpg"/>
          </div>      
        </footer>
      </>
    )
  }
}
