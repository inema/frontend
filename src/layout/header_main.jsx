import React, { Component } from 'react'
import { Link } from "react-router-dom";
import InputSearchHeader from "../components/form/input_search_header";
import OrgaoContext from '../context'
import { SmallArrow } from '../components/own_icons'
import LinkFlex from '../components/link_flex'
import LinkOrgao from '../components/link_orgao'
import BarraGovernoBA from '../components/barra_governo_ba'

export default class HeaderMain extends Component {
  
  constructor(props){
    super(props)    

    this.state = {
      menuIntranetIsOpened : false,
      toggleMenuIntranet : () => this.state.menuIntranetIsOpened ? this.setState({    
          menuIntranetIsOpened : false,
          menuOuvidoriaIsOpened : false,
          menuConsultaIsOpened: false
        }) : 
        this.setState({ 
          menuIntranetIsOpened : true,
          menuOuvidoriaIsOpened : false,
          menuConsultaIsOpened: false
        }),       
      menuOuvidoriaIsOpened : false,
      toggleMenuOuvidoria : () => this.state.menuOuvidoriaIsOpened ? this.setState({ 
          menuOuvidoriaIsOpened : false,
          menuIntranetIsOpened: false,
          menuConsultaIsOpened: false
        }) : 
        this.setState({ 
          menuOuvidoriaIsOpened : true,
          menuIntranetIsOpened: false,
          menuConsultaIsOpened: false
        }),
      menuConsultaIsOpened : false,
      toggleMenuConsulta : () => this.state.menuConsultaIsOpened ? this.setState({ 
          menuConsultaIsOpened: false,
          menuOuvidoriaIsOpened : false,
          menuIntranetIsOpened: false
        }) : 
        this.setState({ 
          menuConsultaIsOpened: true,
          menuOuvidoriaIsOpened : false,
          menuIntranetIsOpened: false 
        }),
      menu_intranet: []    
    }
  }

  loadMenuIntranet = () => {
    fetch(`${process.env.REACT_APP_WP_API}/wp-json/intranet_config/v1/menus/intranet`).then(
      (response) => response.json() ).then(
        (r) => {
          // Primeiro deve-se reordenar o array de response 'r'
          const tempLevel1 = []
          r.forEach( e => {
            if(e.level == 1) tempLevel1.push(e)
          })

          // Reordena os level 1 para em caso de troca Up e Down entre categorias
          tempLevel1.sort( function(a, b){
            a.order = parseInt(a.order)
            b.order = parseInt(b.order)
            if( a.order > b.order ) return 1
            if( a.order < b.order ) return -1
            return 0
          })

          let orderedTemp = []
          // Reordena seguindo a ordem level/order > children
          tempLevel1.forEach( e => {
            orderedTemp.push(e)
            orderedTemp = this.addChildrenInOrder(e.id, orderedTemp, r)
            // console.log(orderedTemp)
          })

          this.setState({ menu_intranet : orderedTemp });
      })
  }

  // Coloca em ordem o menu recebido pela API 
  addChildrenInOrder = (parentId, arr, temp) => {
    // console.log("temp", temp)
    
    const tempChildren = []
    temp.forEach( (e, i, a) => {
      if(e.child_of == parentId){
        // console.log("Adicionado inicialmente elemento" + e.nome + "nos filhos de " + parentId)
        tempChildren.push(e)
      }
    })

    // Reorganizar array com base na ordem
    tempChildren.sort( function(a, b){
      a.order = parseInt(a.order)
      b.order = parseInt(b.order)
      if( a.order > b.order ) return 1
      if( a.order < b.order ) return -1
      return 0
    })

    // pesquisa se também tem algum filho desse que acabou de achar, caso não, pressegue, caso sim cama a própria função novamente
    tempChildren.forEach( e => {
      arr.push(e)
      temp.forEach( el => {
        // Verifico se tem filhos desse item recem adicionado em TEMP
        if(el.child_of == e.id){
          // console.log("HEY! SOU FILHO DO FILHO DE "+parentId+" SOU "+el.id)
          let shouldAdd = true // Depois de perceber que esse loop sempre duplica os filhos > level 2 adicionei esse hack limitador, isso acontee pois logo no início da função ele já adiciona no tempChildren e no loop adiciona novamente evoluindo exponencialmente
          // console.log("arr", arr)
          arr.forEach( elem => {
            if(elem.id == el.id) shouldAdd = false
          })
          if(shouldAdd) this.addChildrenInOrder(e.id, arr, temp)
        }
      })
    })

    return arr
  }

  componentDidMount(){
    // Já que todas as páginas tem HEADER aqui verifica-se o orgao da url, e, dependendo se baste com o context ou não se modifica sempre a url
    // {this.props.match.params.orgao ? this.props.match.params.orgao : "inema"}
    // console.log(this.props.context.orgao);    

    this.loadMenuIntranet()
  }

  checkLastParent = index => {
    this.state.menu_intranet.forEach( (e, i, a) => {
      // Caso o index bata com o i avalia se pai != null, caso sim, retorna child of, caso não pega o pai e chama checkLasParent com o index do pai.

      console.log("index", index)
    })
  }

  render() {    
    return (
      <OrgaoContext.Consumer>        
        {context => (
          <>
          <BarraGovernoBA />
        <header className="main">
          <div className="container">
            <h1><Link className={`__${context.orgao}`} to="/">INTRANET DO MEIO AMBIENTE</Link></h1>
            <div id="selecionar_orgao">
              <ul className="ul_inline">
                <li onClick={context.alterarOrgao} data-orgao="inema" className={ context.orgao == "inema" ? "borda_orgao __inema selecionado" : null}>INEMA</li>
                <li onClick={context.alterarOrgao} data-orgao="sema" className={ context.orgao == "sema" ? "borda_orgao __sema selecionado" : null}>SEMA</li>
              </ul>
            </div>

            {/* MENU PRINCIPAL */}
            <nav className="menu_principal">
              <ul className="ul_inline">
                <li className="select"><span className={`select_span marged __${context.orgao}`} onClick={this.state.toggleMenuIntranet}>Menu Intranet <SmallArrow className={`cor_orgao __${context.orgao}`}/></span>

                {/* MENU INTRANET */}
                { this.state.menuIntranetIsOpened ?
                  <div id="menu_intranet">
                    <ul>
                      {this.state.menu_intranet.map( (e, i, a) => {

                      // Variável atribuida para verificação de ultima linha da lista de menu para poder atribuir margem inferior 
                      let lastItem = false

                      if(e.level == 1){
                        // Verifica se tem filho, se não tiver atribui classe mb que atribui margem inferior
                        let catHasChildren = false
                        this.state.menu_intranet.forEach( el => {
                          if(el.level == 2 && el.child_of == e.id) catHasChildren = true;
                        })

                        console.log(e)
                      return <li data-level={e.level} data-order={e.order} className={`titulo cor_orgao __${context.orgao} ${catHasChildren ? null : "mb" }`}>
                        <LinkFlex
                          to={e.src}
                          className="titulo"
                          orgao={e.orgao} >{e.nome}</LinkFlex></li>
                      }else{
                        // verifica se é o ultimo item, ou seja, se o próximo item da lista é um level1 então ele é o ultimo, se sim atribui uma classe mb
                        if(a[i+1]){
                          if(parseInt(a[i+1].level) == 1){
                            lastItem = true;
                          } 
                        }
                        // verifica, se for level 3, se ele tem algum filho
                        if(e.level == 3){
                          let level3sons = []
                          this.state.menu_intranet.forEach( el => {
                            if(el.level == 4 && el.child_of == e.id) level3sons.push(el)
                          })
                          if(level3sons.length){
                            console.log(level3sons)
                            return ( <div className="hoverable_item_wrap"><li className="subitem hoverable" data-level={e.level} data-order={e.order}>
                              <LinkFlex 
                                to={e.src}
                                orgao={e.orgao} >{e.nome} <SmallArrow/></LinkFlex></li>
                            <ul>
                              {level3sons.map( elem => {
                                return <li data-level={elem.level} data-order={elem.order}>
                                  <LinkFlex 
                                    to={elem.src}
                                    orgao={e.orgao} >{elem.nome}</LinkFlex></li>
                                }
                              )}
                            </ul></div> )
                          }
                        }

                        if(e.level != 4){
                          return <li className={`${e.level == 3 ? "subitem" : ''} ${lastItem ? "mb" : ''}`} data-level={e.level} data-order={e.order}>
                            <LinkFlex 
                              to={e.src}
                              orgao={e.orgao} >{e.nome}</LinkFlex></li>
                        }else{
                          return null
                        }
                      }
                      })}
                    </ul>
                  </div> : null }

                </li>
                <li className="select"><span className={`select_span inline __${context.orgao}`} onClick={this.state.toggleMenuConsulta}>Consulta <SmallArrow color="#ccc" className={`cor_orgao __${context.orgao}`}/></span>
                  {/* MENU CONSULTA */}
                  { this.state.menuConsultaIsOpened ?
                  <div id="menu_consulta">
                    <ul>
                      <li><a href="http://www.meioambiente.ba.gov.br/modules/conteudo/conteudo.php?conteudo=321" className={`__${context.orgao}`} target="_blank">Legislação</a></li>
                      <li><a href="http://sol.inema.ba.gov.br/sol/servicos/consultaProcesso/" className={`__${context.orgao}`} target="_blank">Processos</a></li>
                      <li><a href="http://www.seia.ba.gov.br/fiscalizacao/consulta-outorga" className={`__${context.orgao}`} target="_blank">Outorgas</a></li>
                      <li>Procedimentos</li>
                      <li>Formulário</li>
                      <li><LinkOrgao to="sites-relacionados">Sites Relacionados</LinkOrgao></li>
                    </ul>
                  </div> : null }
                </li>
                <li><LinkOrgao to="fale-conosco">Fale Conosco</LinkOrgao></li>
                <li className="select"><span className={`select_span inline __${context.orgao}`} onClick={this.state.toggleMenuOuvidoria}>Ouvidoria <SmallArrow color="#ccc" className={`cor_orgao __${context.orgao}`}/></span>
                  {/* MENU OUVIDORIA */}
                  { this.state.menuOuvidoriaIsOpened ?
                  <div id="menu_ouvidoria">
                    <ul>
                      <li><a target="_blank" href="http://www.tag.ouvidoriageral.ba.gov.br/tag/NovaManif.html" className={`__${context.orgao}`} target="_blank">Nova Manifestação</a></li>
                      <li><a target="_blank" href="http://www.tag.ouvidoriageral.ba.gov.br/tag/ConsultaManif.html" className={`__${context.orgao}`} target="_blank">Consulta</a></li>
                      <li><a target="_blank" href="http://www.tag.ouvidoriageral.ba.gov.br/tag/ComplementaManif.html" className={`__${context.orgao}`} target="_blank">Complementação</a></li>
                      <li><a target="_blank" href="http://www.tag.ouvidoriageral.ba.gov.br/tag/ConsultaAtiva.html" className={`__${context.orgao}`} target="_blank">Estatística</a></li>
                      <li className={`cor_orgao __${context.orgao}`}><LinkOrgao to="sobre-ouvidoria">O que é a Ouvidoria</LinkOrgao></li>
                    </ul>
                  </div> : null }
                </li>
              </ul>
            </nav>
            <div id="busca">
              <InputSearchHeader id="pesquisa"/>
            </div>
          </div>
        </header>
        </>
        )}
      </OrgaoContext.Consumer>
    )
  }
}
