import React, { Component } from 'react'
import PageWrap from './page_wrap'
import AsideNoticias from '../components/aside_noticias'

export default function LayoutInstitucional(props) {
  return (
    <PageWrap>
      <section className="institucional_conteudo conteudo_pagina flex">
        <main className="post">
          {props.children}
        </main>
        {/* <AsideNoticias /> */}
      </section>
    </PageWrap>
  )
}