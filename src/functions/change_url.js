/* ----------
  Essa função tem por objetivo rastrear links que estão direcionados internamente, ou seja, que tem a base do link igual ao do da configuração WP e alterar a base para o site react
---------- */

export default async (htmlStr, newUrl) => {

  let wp_base_url = ''

  // Primeiro verifica se existe o registro local da url
  const sess_base_url = sessionStorage.getItem('intranet_base_url')
  if(!sess_base_url){
    console.log(sess_base_url)
    const response = await fetch(`${process.env.REACT_APP_WP_API}/wp-json/wp/v2/site_configs`)
    const data = await response.json()
    wp_base_url = `${data.site_url}/index.php`
    sessionStorage.setItem('intranet_base_url', data.site_url)
  }else{
    wp_base_url = `${sess_base_url}/index.php`
  }
  
  htmlStr = htmlStr.replace(wp_base_url, newUrl )
  console.log("str", wp_base_url, htmlStr)
  return htmlStr
}