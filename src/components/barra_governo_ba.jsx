import React from 'react'

export default () => {
  return (
    <div className="barra_governo_ba">
        <div id="inst-bar">
            <ul id="inst-bar-opts">
                <li id="inst-bar-ba"><a className="barra_governo_ba" href="http://www2.ba.gov.br/" target="_blank">Governo do Estado da Bahia</a></li> 
                <li id="inst-bar-opt-ba"><a className="barra_governo_ba" href="http://www2.ba.gov.br/modules/conteudo/conteudo.php?conteudo=6" target="_blank">Sites do Governo</a></li>
                <li id="inst-bar-opt-transparencia"><a className="barra_governo_ba" href="http://www.ba.gov.br/node/1050" target="_blank">Transpar&ecirc;ncia</a></li>
                <li id="inst-bar-opt-ouvidoria"><a className="barra_governo_ba" href="http://www.ouvidoriageral.ba.gov.br" target="_blank">Ouvidoria Geral</a></li>
                <li id="inst-bar-opt-acesso-informacao"><a className="barra_governo_ba" href="http://www.acessoainformacao.ba.gov.br" target="_blank">Acesso &agrave; Informa&ccedil;&atilde;o</a></li> 
                <li id="inst-bar-opt-redes-sociais"><a className="barra_governo_ba" href="http://www2.ba.gov.br/modules/conteudo/conteudo.php?conteudo=9" target="_blank">Redes Sociais Governo</a></li>
            </ul>
        </div>
    </div>
  )
}
