import React, { useContext } from 'react'
import OrgaoContext from '../context'
import { Link } from "react-router-dom";

export default props => {

  const context = useContext(OrgaoContext);

  return (
    <Link to={`/${context.orgao}/${props.to}`} className={`__${context.orgao} ${props.className ?props.className : ""}`}>{props.children}</Link>
  )
}
