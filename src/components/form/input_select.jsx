import React, { Component } from 'react';

class Select extends Component {
  constructor(props) {
    super(props)

    this.state = {
      focused_or_filled: false
    }

    this.style = {
      backgroundImage: "url('img/icones/seta.svg')"
    }

    this.label = React.createRef();
    this.input = React.createRef();
  }

  // Modifica state.focus para true
  onFocus = (e) => {
    console.log("oi");
    
    this.setState({ focused_or_filled: true }, this.setLabelStyle);
  }

  // Modifica state.focus para false caso esteja vazio
  onBlur = (e) => {    
    if (!e.target.value) {
      this.setState({ focused_or_filled: false }, this.setLabelStyle);
    }
  }

  // Adiciona ou remove classe css baseado em state.focus
  setLabelStyle = () => {
    if (this.state.focused_or_filled) {
      this.label.current.classList.add("focused_or_filled");
      this.input.current.classList.add("focused_or_filled");
    } else {
      this.label.current.classList.remove("focused_or_filled");
      this.input.current.classList.remove("focused_or_filled");
    }
  }

  render() {
    return (
      <div className="input_wrap">
        <label for={this.props.id} ref={this.label} onClick={ () => this.input.current.click() }>{this.props.placeholder}</label>
        <select className="select" id={this.props.id} name={this.props.id} ref={this.input} className={this.props.className} onMouseDown={this.onFocus} onBlur={this.onBlur} onChange={this.onBlur} style={this.style}>
          <option value="" selected disabled></option>
          {this.props.children}
        </select>
      </div>
    );
  }
}

export default Select;