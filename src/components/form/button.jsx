import React, { Component } from 'react'

export default class Button extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <button className={this.props.className} type={this.props.type} disabled={this.props.disabled}>{this.props.children}</button>
    )
  }
}
