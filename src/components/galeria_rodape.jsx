import React, { Component } from 'react'
import ImageGallery from 'react-image-gallery';
import "react-image-gallery/styles/css/image-gallery.css";
import Modal from '@material-ui/core/Modal';

export default class GaleriaRodape extends Component {

  state = {
    items: [],
    modalIsOpened: false,
    startIndex: 0
  }

  handleOpen = index => e => {
    e.preventDefault();
    this.setState({ modalIsOpened: true, startIndex: index });
  };

  handleClose = () => {
    this.setState({ modalIsOpened: false });
  };

  componentDidMount(){
    let items = []
    this.props.items.forEach( e => {
      items.push({
        original: e,
        thumbnail: e
      })
    })

    this.setState({ items })
  }

  render() {
    return [
      <Modal
        id="modal_galeria" 
        open={this.state.modalIsOpened}
        onClose={this.handleClose}
        >
        <ImageGallery 
        items={this.state.items}
        startIndex={this.state.startIndex} />
      </Modal>,
      <section className="galeria_rodape">
        {this.props.items.map( (e, i) => 
          <div onClick={this.handleOpen(i)} style={{ backgroundImage: `url("${e}")`}}>
          </div>
        )}
      </section>
    ]
  }
}
