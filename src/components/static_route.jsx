import React, { Component } from 'react'

import StaticPage from '../pages/static_page'
import FaleConosco from '../pages/fale_conosco'


function isEmpty(obj) {
  for(var key in obj) {
      if(obj.hasOwnProperty(key))
          return false;
  }
  return true;
}

export default class StaticRoute extends Component {

  // Referências criada para caso seja necessário mais de um layout para páginas estáticas
  //  se tornou redundante após uso de ' || StaticPage ' em this.state.component
  slugs = {
    // Institucional
    historico: StaticPage,
    titulares: StaticPage,
    unidades: StaticPage,
    "orgaos-colegiados" : StaticPage,
    "nosso-papel" : StaticPage,
    "marcas-manuais": StaticPage,
    // Nossa Equipe
    "ver-de-dentro": StaticPage,
    "altos-papos": StaticPage,
    // Consulta
    "fale-conosco": FaleConosco,
    "sites-relacionados": StaticPage,
    // Programas e Projetos
    "educacao-ambiental": StaticPage,
    "socioeconomia-biodiversidades": StaticPage,
  }

  json_data_404 = {
    title: {
      rendered : "Página não existente..."
    },
    content: {
      rendered: "Conteúdo não existente..."
    }
  }

  constructor(props){
    super(props);

    this.state = {
      component: this.slugs[this.props.routeProps.match.params.pagina] || StaticPage,
      slug: this.props.routeProps.match.params.orgao + "-" + this.props.routeProps.match.params.pagina
    }
  }

  load_page = () => {

    const slug_orgao = this.props.routeProps.match.params.orgao + "-" + this.props.routeProps.match.params.pagina;
    const slug = this.props.routeProps.match.params.pagina;

    // Verifica se tem página de conteúdo específico por órgão
    fetch(`${process.env.REACT_APP_WP_API}/wp-json/wp/v2/pages?slug=${slug_orgao}`).then(
    (response) => response.json() ).then(
      (r) => {
        if(r.length){
          console.log(r)
          if(this.props.context.json_data.id !== r[0].id) this.props.context.updateJsonData(r[0])
        }else{
          // Carrega o conteúdo comum
          fetch(`${process.env.REACT_APP_WP_API}/wp-json/wp/v2/pages?slug=${slug}`).then(
            (response) => response.json() ).then(
              (r) => {
                if(r.length){
                  console.log(r)
                  if(this.props.context.json_data.id !== r[0].id){
                    this.props.context.updateJsonData(r[0])
                  }
                }
              }
            )   
        }
      }
    )
  }

  componentDidMount(){
    this.mounted = true;
    
    if(this.props.context.routeProps.location !== this.props.routeProps.location){
      this.props.context.changeRouteProps(this.props.routeProps)
    }

    console.log("*****Pagina atual", this.props.routeProps.location.pathname, )
    console.log("*****CONTEXT VAZIO?", isEmpty(this.props.context.routeProps), this.props.context )

    // Verifica órgão passado pela URL e atribui ao orgao do context caso seja diferente
    if(this.props.routeProps.match.params.orgao !== this.props.context.orgao){
      this.props.context.alterarOrgao();
    }

    if(isEmpty(this.props.context.routeProps)){
      // Não existe nenhuma propriedade de rota no context, então deve-se criar uma página em json_data
      this.load_page();
    }else if(this.props.routeProps.location.pathname !== this.props.context.routeProps.location.pathname){
      // Existe já um registro de página no context, mas essa é outra
      this.load_page();
    }
  }

  componentWillUnmount(){
    this.mounted = false;    
  }

  render() {

    const ComponentName = this.state.component;
    // Passa o slug que foi utilizado na busca

    return  <ComponentName 
      context={this.props.context} 
      data={this.props.context.json_data}
      />

  }
}