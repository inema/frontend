import React, { Component } from 'react'
import { Link } from "react-router-dom"
import ReactHtmlParser from 'react-html-parser';
import OrgaoContext from '../context'

export default class AsideNoticias extends Component {

  static contextType = OrgaoContext;
  
  render() {

    return (
      <aside>
        <h2>Notícias</h2>
        {this.props.conteudo.map( (v, i) => {
          const max_side_posts = 3;

          if(i < max_side_posts){
            let titulo = v.title.rendered,
            dataObj = new Date(Date.parse(v.date)),
            mes = dataObj.getMonth()+1,
            data = dataObj.getDate()+"/"+mes+"/"+dataObj.getFullYear(),
            trecho = v.excerpt.rendered;

            return (
              <Link to={`${v.slug}`} onClick={this.forceUpdate} className={`__${this.context.orgao}`}>
              <div className="trecho_noticia">
                {v.better_featured_image ? <div className="img_destaque" style={{ backgroundImage: `url("${v.better_featured_image ?v.better_featured_image.media_details.sizes.thumbnail ? v.better_featured_image.media_details.sizes.thumbnail.source_url : v.better_featured_image.source_url : ''}")` }}/> : null}
                <time>{data}</time>
                <span className="titulo">{titulo}</span>
                <span className="trecho">{ReactHtmlParser(trecho)}</span>
                <a className="leia_mais">Leia mais</a>
              </div>
              </Link>
            )
          }
        })}
      </aside>
    )
  }
}
