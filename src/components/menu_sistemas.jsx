import React, { Component } from 'react'
import OrgaoContext from '../context'
import { Etc } from '../components/own_icons'
import { Link } from 'react-router-dom'
import LinkFlex from '../components/link_flex'

export default class MenuSistemas extends Component {

  static contextType = OrgaoContext;

  constructor(props) {
    super(props)

    this.state = {
      menuIsOpened: false,
      toggleMenu: () => {
        this.state.menuIsOpened ? this.setState({ menuIsOpened: false }) : this.setState({ menuIsOpened: true })
      },
      destaques: [],
      menu_sistemas: []
    }
  }

  loadMenuSistemas = () => {
    fetch(`${process.env.REACT_APP_WP_API}/wp-json/intranet_config/v1/menus/sistemas`).then(
      (response) => response.json()).then(
        (r) => {
          // Primeiro pega logo os destaques e remove eles do array
          this.setState({ destaques: r.destaques }, () => console.log("$$$$", this.state))
          delete r.destaques

          let obj_to_array = [];
          for (var key in r) {
            obj_to_array.push(r[key]);
          }

          r = obj_to_array
          console.log("$$$$ r", r)

          // Com o que restou Primeiro deve-se reordenar o array de response 'r'
          const tempLevel1 = []
          r.forEach(e => {
            console.log("$$$$ level", e.level == 1)
            if (e.level == 1) tempLevel1.push(e)
          })

          // Reordena os level 1 para em caso de troca Up e Down entre categorias
          tempLevel1.sort(function (a, b) {
            a.order = parseInt(a.order)
            b.order = parseInt(b.order)
            if (a.order > b.order) return 1
            if (a.order < b.order) return -1
            return 0
          })

          let orderedTemp = []
          // Reordena seguindo a ordem level/order > children
          tempLevel1.forEach(e => {
            orderedTemp.push(e)
            orderedTemp = this.addChildrenInOrder(e.id, orderedTemp, r)
            console.log(orderedTemp)
          })
          console.log("$$$$ tempLevel", tempLevel1)
          console.log("$$$$ orderedTemp", orderedTemp)

          this.setState({ menu_sistemas: orderedTemp }, () => console.log(this.state));
        })
  }

  // Coloca em ordem o menu recebido pela API
  addChildrenInOrder = (parentId, arr, temp) => {
    console.log("temp", temp)

    const tempChildren = []
    temp.forEach((e, i, a) => {
      if (e.child_of == parentId) {
        console.log("Adicionado inicialmente elemento" + e.nome + "nos filhos de " + parentId)
        tempChildren.push(e)
      }
    })

    // Reorganizar array com base na ordem
    tempChildren.sort(function (a, b) {
      a.order = parseInt(a.order)
      b.order = parseInt(b.order)
      if (a.order > b.order) return 1
      if (a.order < b.order) return -1
      return 0
    })

    // Adiciona itens temporarios ao raiz
    tempChildren.forEach(e => arr.push(e))
    console.log(tempChildren)

    // pesquisa se também tem algum filho desse que acabou de achar, caso não, pressegue, caso sim cama a própria função novamente
    tempChildren.forEach(e => {
      temp.forEach(el => {
        // Verifico se tem filhos desse item recem adicionado em TEMP
        if (el.child_of == e.id) {
          console.log("HEY! SOU FILHO DO FILHO DE " + parentId + " SOU " + el.id)
          let shouldAdd = true // Depois de perceber que esse loop sempre duplica os filhos > level 2 adicionei esse hack limitador, isso acontee pois logo no início da função ele já adiciona no tempChildren e no loop adiciona novamente evoluindo exponencialmente
          console.log("arr", arr)
          arr.forEach(elem => {
            if (elem.id == el.id) shouldAdd = false
          })
          if (shouldAdd) this.addChildrenInOrder(e.id, arr, temp)
        }
      })
    })

    return arr
  }

  componentDidMount() {
    this.loadMenuSistemas()
  }

  render() {

    const context = this.context

    return (
      <nav id="menu_sistema" className={`bkg_orgao __${context.orgao}`}>
        <div className={`bkg_orgao __${context.orgao} mais`} onClick={this.state.toggleMenu}><Etc /></div>
        <ul className="main">
          {this.state.destaques.map(e => <li><LinkFlex to={e.src}>{e.nome}</LinkFlex></li>)}
        </ul>
        <div className={`apendice __${context.orgao} ${this.state.menuIsOpened ? "__opened" : "__closed"}`}>
          {this.state.menu_sistemas.map((e, i) => {
            if (e.level == 1) {
              return (
                <div>
                  <header className={`titulo cor_orgao __${context.orgao}`}>{e.nome}</header>
                  <ul>
                    {this.state.menu_sistemas.map(el => {
                      if (el.child_of == e.id) {
                        return <li><LinkFlex
                          to={el.src}
                          className="titulo">{el.nome}
                        </LinkFlex></li>
                      }
                    })}
                  </ul>
                </div>
              )
            }
          })}
        </div>
      </nav>
    )
  }
}
