import React, { useContext } from 'react'
import OrgaoContext from '../context'
import { Link } from "react-router-dom";

export default props => {

  /**
   * Criação de link flexível que aceita tanto links esternos quanto redirecionamento para páginas externas, aceitando tbm os shortcodes {ORGAO} e {WP} criados no HTML
   */

  console.log(props)

  const context = useContext(OrgaoContext);

  let href = props.to
  let orgao = props.orgao ? parseInt(props.orgao) === 0 ? "inema" : parseInt(props.orgao) === 1 ? "sema" : "ambos" : "ambos"

  let tipo = "linkInterno"
  // Verifica se existe a página relativa {WP} no src
  if (href.indexOf("{WP}") > -1) {
    href = href.replace("{WP}", process.env.REACT_APP_WP_FOLDER)
    tipo = "wpFolder"
  }

  // Verifica tag de órgao {ORGAO}
  if (href.indexOf("{ORGAO}") > -1) {
    href = href.replace("{ORGAO}", context.orgao)
  }

  // verifica se existe encaminhamento absoluto com https:// ou http://
  if (href.indexOf("http://") > -1 || href.indexOf("https://") > -1) {
    tipo = "linkExterno"
  }

  // console.log("$$$$ linkOrgao", href, tipo)

  if (orgao === "ambos" || orgao === context.orgao) {
    if (tipo === "linkInterno") {
      return (
        <Link
          to={`/${context.orgao}/${props.to}`}
          className={`__${context.orgao} ${props.className ? props.className : ""}`}>{props.children}</Link>
      )
    } else {
      return (
        <a
          href={href}
          target="_blank"
          className={`__${context.orgao} ${props.className ? props.className : ""}`}>{props.children}</a>
      )
    }
  } else {
    return null
  }
}
