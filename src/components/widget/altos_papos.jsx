import React, { Component } from 'react'
import OrgaoContext from '../../context'
import { Link } from "react-router-dom";
import ReactHtmlParser from 'react-html-parser';

export default class DestaqueDoMesWidget extends Component {

  static contextType = OrgaoContext;

  state = {
    artigo : {
      title: {
        rendered: "Carregando..."
      },
      excerpt: {
        rendered: "Carregando..."
      },
      slug: '',
      meta: {
        _nome_entrevistado: "Carregando..."
      },
      better_featured_image: {
        source_url: ''
      }
    }
  }

  loadItems = () => {
    fetch(`${process.env.REACT_APP_WP_API}/wp-json/wp/v2/altos-papos?per_page=1`).then(
      (response) => {
        return response.json() 
      }).then(
        r => {
          console.log("$$$", r[0])
          this.setState({ artigo : r[0] });
    })
  }

  componentDidMount(){
    this.loadItems()
  }

  // código adaptado de https://awik.io/determine-color-bright-dark-using-javascript/
  isDark = arrayColor => {

    // Variables for red, green, blue values
    let r = arrayColor[0], 
    g = arrayColor[1],
    b = arrayColor[2], 
    hsp = 0;
    
    // HSP (Highly Sensitive Poo) equation from http://alienryderflex.com/hsp.html
    hsp = Math.sqrt(
    0.299 * (r * r) +
    0.587 * (g * g) +
    0.114 * (b * b)
    );

    // Using the HSP value, determine whether the color is light or dark
    if (hsp>127.5) {
      return false;
    }

    return true;
  }

  render(){

    console.log(this.state.artigo.palette)
    const props = this.props,
    context = this.context,
    palette = this.state.artigo.palette ? this.state.artigo.palette[1] : [
      0, 0, 0
    ],
    coloredStyle = {
      backgroundColor: `rgb(${palette[0]},${palette[1]},${palette[2]})`,
      boxShadow: `15px 0 0 rgb(${palette[0]},${palette[1]},${palette[2]}), -15px 0 0 rgb(${palette[0]},${palette[1]},${palette[2]})`
    }

    return (
      <section id="altos_papos" style={{ 
          order: props.order,
          backgroundImage: `url(${this.state.artigo ? this.state.artigo.better_featured_image ? this.state.artigo.better_featured_image.source_url : null : null})`
        }}>
          <div className={`texto __${this.isDark(palette) ? 'light' : 'dark' }`}>
            <h4 style={coloredStyle}><strong>ALTOS PAPOS</strong> com {this.state.artigo.meta._nome_entrevistado}</h4>
            <div/>
            <span style={coloredStyle}><Link to={`${context.orgao}/altos-papos/${this.state.artigo.slug}`}>{ReactHtmlParser(this.state.artigo.excerpt.rendered)}</Link></span>
          </div>
        
      </section>
    )
  }
}
