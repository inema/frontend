import React, { Component } from 'react'
import OrgaoContext from '../../context'
import { Link } from 'react-router-dom'
import ReactHtmlParser from 'react-html-parser';

export default class WidgetFicaDica extends Component {

  static contextType = OrgaoContext;

  state = {
    item : {
      title: {
        rendered: "Carregando..."
      },
      excerpt: {
        rendered: "Carregando..."
      },
      slug: ''
    }
  }

  loadItems = () => {
    fetch(`${process.env.REACT_APP_WP_API}/wp-json/wp/v2/fica-a-dica?per_page=1`).then(
      (response) => {
        return response.json() 
      }).then(
        r => {
          console.log("$$$", r[0])
          this.setState({ item : r[0] });
    })
  }

  componentDidMount(){
    this.loadItems()
    console.log("$$$ teste")
  }

  render(){
    const item = this.state.item,
    props = this.props,
    context = this.context

    return (
      <section id="fica_dica" style={{ order: props.order }}>
          <div className="tema">{item.meta ? item.meta._categoria_fad.toUpperCase() : null}</div>
          <Link to={item.slug ? `${context.orgao}/fica-a-dica/${item.slug}` : '' }><h1 className="titulo">{item.title.rendered.toUpperCase()}</h1></Link>
          <Link to={`${context.orgao}/fica-a-dica`} className={`secao cor_orgao __${context.orgao}`}>FICA DICA</Link>
          <Link to={item.slug ? `${context.orgao}/fica-a-dica/${item.slug}` : '' }>{ReactHtmlParser(item.excerpt.rendered)}</Link>
      </section>
    )
  }
}
