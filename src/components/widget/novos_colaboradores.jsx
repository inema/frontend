import React, { Component } from 'react'
import Carousel from 'nuka-carousel';
import OrgaoContext from '../../context'
import { Link } from "react-router-dom";
import { LeftArrowNovosColaboradores, RightArrowNovosColaboradores } from '../own_icons'

export default class WidgetNovosColaboradores extends Component {

  static contextType = OrgaoContext;

  constructor(props){
    super(props);

    this.state = {
      slidesToShow : 4,
      colaboradores: []
    }

  }

  componentDidMount(){
    if(window.innerWidth <= 550){
      this.setState({ slidesToShow : 1 })
    }else if(window.innerWidth <= 850){
      this.setState({ slidesToShow : 2 })
    }else if(window.innerWidth <= 1280){
      this.setState({ slidesToShow : 3 })
    }
    this.loadColabs()
  }

  loadColabs = () => {
    fetch(`${process.env.REACT_APP_WP_API}/wp-json/colaboradores/v1/novos-colaboradores`).then(
      (response) => {
        return response.json() 
      } ).then(
        (r) => {
          console.log(r);
          this.setState({ colaboradores : r });
          console.log(this.state);
      })
  }

  render() {
    const context = this.context;

    const allowClick = this.props.allowClick;

    return (
      <section id="novos_colaboradores" className={this.props.className} style={{ order: this.props.order }}>
        <header>
          {this.props.noTitle ? null : <h3 className={`cor_orgao __${context.orgao}`}>NOVOS COLABORADORES</h3>}
        </header>
        <Carousel 
          slidesToShow={this.state.slidesToShow} 
          autoplay={true}
          autoplayInterval={3500}
          wrapAround={true}
          renderBottomCenterControls={null}
          renderCenterLeftControls={({ previousSlide }) => (
          <button onClick={previousSlide}><LeftArrowNovosColaboradores context={context}/></button>
          )}
          renderCenterRightControls={({ nextSlide }) => (
          <button onClick={nextSlide}><RightArrowNovosColaboradores context={context}/></button>
          )}
        >
        {this.state.colaboradores.map( (e, i) => (
          <div className="colaborador_img" data-id={i} onClick={allowClick ? this.props.onClick : null} style={{ backgroundImage: `url("${e.foto ? e.foto.path : '' }")` }}>
            <Link to={`/${this.context.orgao}/novos-colaboradores/${e.id}`}>
              <div className={`cortina bkg_orgao __${context.orgao}`}/>
              <div className="conteudo">
                <h4>{e.nome}</h4>
                <span className="funcao">{e.setor}</span>
                <span className="orgao">{e.orgao}</span>
              </div>
            </Link>
          </div>
        ))}
        
        </Carousel>
      </section>
    )
  }
}
