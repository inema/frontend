import React, { Component } from 'react'
import OrgaoContext from '../../context'
import { Link } from "react-router-dom";
import ReactHtmlParser from 'react-html-parser';

export default class WidgetDestaqueDoMes extends Component {

  static contextType = OrgaoContext;

  state = {
    destaque : {
      title: {
        rendered: "Carregando..."
      },
      excerpt: {
        rendered: "Carregando..."
      },
      slug: ''
    }
  }

  loadItems = () => {
    fetch(`${process.env.REACT_APP_WP_API}/wp-json/wp/v2/destaque-do-mes?per_page=1`).then(
      (response) => {
        return response.json() 
      }).then(
        r => {
          console.log("$$$", r[0])
          this.setState({ destaque : r[0] });
    })
  }

  componentDidMount(){
    this.loadItems()
  }

  render(){

    const props = this.props,
    context = this.context

    return (
      <section id="destaque" style={{ order: props.order }}>
        <header>
          <Link to={this.state.destaque.slug ? `${context.orgao}/destaque-do-mes/${this.state.destaque.slug}` : '' }><h1>{this.state.destaque.title.rendered}</h1></Link>
        </header>
        <a className={`cor_orgao __${context.orgao}`} href={`${context.orgao}/destaque-do-mes`} >DESTAQUE DO MÊS</a>
        <Link to={`${context.orgao}/destaque-do-mes/${this.state.destaque.slug}`}>{ReactHtmlParser(this.state.destaque.excerpt.rendered)}</Link>
      </section>
    )
  }

}
