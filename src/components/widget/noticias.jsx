import React, { Component } from 'react'
import Carousel from 'nuka-carousel'
import OrgaoContext from '../../context'
import ReactHtmlParser from 'react-html-parser';
import { Link } from "react-router-dom";
import { LeftSmallArrowNoticias, RightSmallArrowNoticias } from '../own_icons'

export default class WidgetNoticias extends Component {

  static contextType = OrgaoContext;

  constructor(props){
    super(props)

    this.state = {
      noticias: [
        {
          title: {
            rendered: "Carregando..."
          },
          content: {
            rendered: "Carregando..."
          }
        }
      ]
    }
  }

  loadPosts = () => {
    fetch(`${process.env.REACT_APP_WP_API}/wp-json/wp/v2/posts?per_page=5&page=1`).then(
      (response) => {
        // console.log(response.headers.get("X-WP-Total"))
        this.setState({ paginas: parseInt(response.headers.get("X-WP-TotalPages")) });
        return response.json() 
      } ).then(
        (r) => {
          console.log(r);
          this.setState({ noticias : r });
          console.log(this.state);
      })
  }

  componentDidMount(){
    this.loadPosts()
  }

  render(){
    return (
      <section id="noticias" style={{ order: this.props.order }}>
        <header></header>
        <Carousel
          autoplay={true}
          autoplayInterval={10000}
          wrapAround={true}
          renderCenterLeftControls={({ previousSlide }) => (
          <button onClick={previousSlide}><LeftSmallArrowNoticias/></button>
          )}
          renderCenterRightControls={({ nextSlide }) => (
          <button onClick={nextSlide}><RightSmallArrowNoticias/></button>
          )}
        >
          {this.state.noticias.map( e => (
            <Link to={`/${this.context.orgao}/noticias/${e.slug}`}>
              <div className="noticia_slide" style={{ backgroundImage: `url(${e.better_featured_image ? e.better_featured_image.source_url : '' })` }}>
                <div className={`cortina __${this.context.orgao}`}></div>
                <div className="trecho">
                  <h3>{e ? e.title.rendered : null}</h3>
                  <p>{e.excerpt ? ReactHtmlParser(e.excerpt.rendered) : null}</p>
                </div>
              </div>
            </Link>
          ))}
        </Carousel>
      </section>
    )
  }
}
