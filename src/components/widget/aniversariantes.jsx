import React, { Component } from 'react'
import OrgaoContext from '../../context'
import { AniversariantesWidgetArrow } from '../../components/own_icons'
import { Link } from 'react-router-dom'

export default class WidgetAniversariantes extends Component {

  static contextType = OrgaoContext;

  constructor(props){
    super(props)

    const date = new Date()
    this.state = {
      aniversariantes: [],
      data: {
        dia: date.getDate(),
        mes: date.getMonth()+1
      },
      boxStyle: {
        height: 0,
        width: 0
      },
      daysList: {},
      menuIsOpened: false
    }

    this.main = React.createRef()
  }

  loadAniversariantes = () => {
    fetch(`${process.env.REACT_APP_WP_API}/wp-json/colaboradores/v1/aniversariantes/${this.state.data.mes}/${this.state.data.dia}`).then(
      (response) => {
        // console.log(response.headers.get("X-WP-Total"))
        this.setState({ paginas: parseInt(response.headers.get("X-WP-TotalPages")) });
        return response.json() 
      } ).then(
        (r) => {
          console.log(r);
          this.setState({ aniversariantes : r }, this.setSize );
          console.log(this.state);
      })
  }

  setSize = () => {
    const wrapDivH = this.main.current.clientHeight
    const wrapDivW = this.main.current.clientWidth
    const numBox = this.state.aniversariantes.length
    let boxStyle = {}

    if(numBox === 1) boxStyle = {
      width: wrapDivW,
      height: wrapDivH
    }

    if(numBox === 2) boxStyle = {
      width: wrapDivW/2.08,
      height: wrapDivH
    }

    if(numBox === 3) boxStyle = {
      width: wrapDivW/3.08,
      height: wrapDivH
    }

    if(numBox >= 4) boxStyle = {
      width: wrapDivW/4.2,
      height: wrapDivW/4.2
    }

    this.setState({ boxStyle })

    console.log("$$$$", this.main.current.clientHeight, this.main.current.clientWidth, this.state.aniversariantes.length)
  }

  meses = [
    "", "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"
  ]

  componentDidMount(){
    this.loadAniversariantes()
    let aniversariantes = []
    for (let i = 0; i < 6; i++) {
      aniversariantes.push({ 
        nome: "Filipe L",
        foto: {},
        mostrar_foto: 1
      })      
    }

    this.setState({
      aniversariantes
    }, this.setSize )

    // Listando todos os dias dos meses
    const ano = new Date().getFullYear()

    let daysList = {}
    this.meses.map( (m, i) => {
      if(m !== ""){
        daysList[m] = []
        let n_dias = new Date(ano, i, 0).getDate()
        for (let d = 1; d <= n_dias; d++) {
          daysList[m].push(d)         
        }
      }
    })
    console.log("$$$$", Object.entries(daysList))
    this.setState({ daysList })
  }

  alterarData = (m, d) => e => {
    console.log("$$$$", m+1, d)
    this.setState({
      data: {
        mes: m+1,
        dia: d
      }
    }, this.loadAniversariantes )
    this.toggleMenu()
  }

  toggleMenu = () => {
    this.setState({ menuIsOpened: !this.state.menuIsOpened })
  }


  render(){
    const context = this.context
    const props = this.props
    const daysList = this.state.daysList
    console.log("$$$$",this.meses[this.state.data.mes].substr(0,3).toUpperCase())

    return (
      <section id="aniversariantes" style={{ order: props.order }}>
        <header>
          <h1 className={`cor_orgao __${context.orgao}`}>ANIVERSARIANTES</h1>
          {this.state.menuIsOpened ? 
          <div id="selecao_data">
            <div className="tarja"/>
          {Object.entries(daysList).length ? Object.entries(daysList).map( (e,m) => [
            <h4 className={`cor_orgao __${context.orgao}`}>{e[0]}</h4>,
            <div className="dias">
              {e[1].map( d => <button onClick={this.alterarData(m, d)}>{d}</button>)}
            </div>
          ])
          : null}</div> : null}
          <div 
            id="selecao_data_atual" 
            className={`cor_orgao __${context.orgao}`}
            onClick={this.toggleMenu}
          >{this.state.data.dia} {this.meses[this.state.data.mes].substr(0,3).toUpperCase()} <AniversariantesWidgetArrow context={context} /></div>
        </header>
        <main ref={this.main}>
          {this.state.aniversariantes.length ? this.state.aniversariantes.map( e => (
            <div style={this.state.boxStyle}>
              <div className="background" style={{ backgroundImage: `url("${e.foto ? e.foto.path : '' }")`}}></div>
              <div className="cortina">
                <div className="conteudo">
                  <span><strong>{e.nome}</strong></span>
                  <span>{e.orgao} - {e.setor}</span>
                </div>
              </div>
            </div>
          )) : <div className="message" >Não existem aniversariantes nesse dia</div>}
        </main>
      </section>
    )
  }
}
