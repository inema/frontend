import React, { Component } from 'react'
import Carousel from 'nuka-carousel';
import OrgaoContext from '../../context'
import { LeftArrowAgendaCultural, RightArrowAgendaCultural } from '../own_icons'
import Link from 'react-router-dom/Link';
import ReactHtmlParser from 'react-html-parser'


export default class AgendaCulturalWidget extends Component {
  
  static contextType = OrgaoContext;

  state = {
    eventos: []
  }

  loadItems = () => {
    fetch('http://agendacultural.ba.gov.br/wp-json/wp/v2/posts').then(
      (response) => {
        return response.json() 
      }).then(
        r => {
          console.log("$$$", "AGENDA CULTURAL", r)
          this.setState({ eventos : r });
    })
  }

  componentDidMount(){
    this.loadItems()
  }

  render(){
    const props = this.props
    const context = this.context
    return (
      <section id="agenda_cultural" style={{ order: props.order }}>    
        <header className={`bkg_orgao __${context.orgao}`}>
        <h3>AGENDA CULTURAL</h3>
        </header>
        <Carousel
          autoplay={true}
          autoplayInterval={5000}
          wrapAround={true}
          renderBottomCenterControls={null}
          renderCenterLeftControls={({ previousSlide }) => (
          <button onClick={previousSlide}><LeftArrowAgendaCultural context={context}/></button>
          )}
          renderCenterRightControls={({ nextSlide }) => (
          <button onClick={nextSlide}><RightArrowAgendaCultural context={context}/></button>
          )}
        >
          {this.state.eventos.map( e => {
            // Tentando achar data por regEx
            let data = e.content.rendered.match(/\d{1,2}\s?(de)?\s(\w{3,})\s?(de)?\s?(\d{1,4})?/g)
            // Filtro para remover alguns erros evidente de busca
            if(Array.isArray(data)){
              data = data.filter( v => {
                let aceitos = [ 'Janeiro', 'Fevereiro', 'Março', 'Marco', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro']
                let proibidos = [ 'anos', 'dias', 'meses', 'vagas', 'lugares' ]
                for (let i = 0; i < aceitos.length; i++) {
                  if (v.indexOf(aceitos[i].toLowerCase()) > -1) {
                    return true;
                  }
                }
                return false
              })
            }
            if(!data) data = [ null ]

            // Tentando achar local
            let local = e.content.rendered.match(/(Onde|Local):?\s?[a-zA-Z\u00C0-\u017F_ ]*(<\/p\s?>)?(<br\s?>)?/g)
            if(Array.isArray(local)){
              local.forEach( (e, i, a) => {
                e = e.replace( /(Onde|Local):?\s?/g , '')
                a[i] = e.replace( /(<\/p\s?>)?(<br\s?>)?/g , '')
              })
            }

            if(!local || local.length === 0 || local[0] === "") local = [ "Clique para mais detalhes" ]
            
            return (
              <div className="ag_cultural_slide" style={{
                backgroundImage: `url(${e.better_featured_image ? e.better_featured_image.source_url : null})`
              }}>
                <a target="_blank" href={e.link}><div className="cortina"></div></a>
                <div className="fade"></div>
                <div className="trecho">
                  <h3>{ReactHtmlParser(e.title.rendered)}</h3>
                  <span className="descricao">{ReactHtmlParser(local[0])}</span>
                  <span className={`data cor_orgao __${context.orgao}`}>{data[0]}</span>
                </div>
              </div>
            )
          })}
        </Carousel>
      </section>
    )
  }
}
