import React, { Component } from 'react'
import OrgaoContext from '../../context'
import { Calendario, LeftArrowAgendaCultural, RightArrowAgendaCultural } from '../own_icons'
import Carousel from 'nuka-carousel';
import { Link } from "react-router-dom";


export default class WidgetVemAi extends Component {

  static contextType = OrgaoContext;

  state = {
    calendario: [],
    proxima_data: null,
    proximos_eventos: [],
    mes_referencia: 0,
    ano_referencia: 0,
  }

  loadCalendario = (data=new Date()) => {
    // let data = new Date();
    console.log(data)
    let mes = ("0" + (data.getMonth() + 1)).slice(-2)
    let ano = data.getFullYear()
    fetch(`${process.env.REACT_APP_WP_API}/wp-json/cpt/v1/vem-ai/widget/${ano}/${mes}`).then(
      (response) => {
        return response.json() 
      }).then( (r) => { 
        console.log("$$$$", r)
        this.setState({ 
        calendario : r.calendario_mes,
        mes_referencia : parseInt(r.mes),
        ano_referencia : ano
      }, this.getProximosEventos )})
  }

  prevMonth = () => {
    let data_referencia = new Date(`${this.state.ano_referencia}-${this.state.mes_referencia}-1`)
    let data = new Date(data_referencia.setMonth(data_referencia.getMonth()-1))
    console.log(data)
    this.loadCalendario(data)
  }

  nextMonth = () => {
    let data_referencia = new Date(`${this.state.ano_referencia}-${this.state.mes_referencia}-1`)
    let data = new Date(data_referencia.setMonth(data_referencia.getMonth()+1))
    console.log(data)
    this.loadCalendario(data)
  }

  getProximosEventos = () => {
    console.log("Próximos eventos $$$")
    let d = new Date(),
    day = '' + d.getDate();

    if (day.length < 2) day = '0' + day;

    let proximos_eventos = []
    
    this.state.calendario.map( e => {
      e.map( elem => {
        if(elem){
          if(elem.eventos.length > 0){
            if(parseInt(elem.dia) >= day){
              proximos_eventos.push({
                eventos: elem.eventos,
                data: {
                  dia: elem.dia
                }
              })
            }
          }
        }
      })
    })

    this.setState({
      proximos_eventos: proximos_eventos[0] ? proximos_eventos[0].eventos : [],
      proxima_data: proximos_eventos[0] ? proximos_eventos[0].data : []
    })
  }

  getEventos = (dia, eventos) => () => {
    this.setState({
      proximos_eventos: eventos,
      proxima_data: { dia }
    })
  } 

  componentDidMount(){
    this.loadCalendario()
  }

  render(){

    const context = this.context
    const props = this.props

    const meses = [
      "", "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"
    ]

    const data = new Date()

    return (
      <section className={`bkg_orgao __${context.orgao}`} id="calendario" style={{ order: props.order }}>
        <div>
          <Link to={`${context.orgao}/vem-ai`}>
          <h1>VEM AÍ</h1>
          <h2>Confira aqui o próximo evento</h2>
          </Link>
        </div>
        <div>
          {/* <Calendario /> */}
          <table>
            <thead>
              <tr><td colspan="7" className="caps">
              <button className="prev month_control" onClick={this.prevMonth}><LeftArrowAgendaCultural context={context} /></button>{meses[this.state.mes_referencia]}<button className="next month_control" onClick={this.nextMonth}><RightArrowAgendaCultural context={context} /></button></td></tr>
              <tr>
                <td>D</td><td>S</td><td>T</td><td>Q</td><td>Q</td><td>S</td><td>S</td>
              </tr>
              <tr className="borderbottom" ><td colspan="7"></td></tr>
            </thead>
            <tbody>
              {this.state.calendario.length > 0 ? 
              this.state.calendario.map( e => (
                <tr>{e.map( elem => (
                  <td 
                  className={elem ? `${elem.eventos.length > 0 ? 'bold' : ''} ${parseInt(elem.dia) === data.getDate() && this.state.ano_referencia === data.getFullYear() && this.state.mes_referencia === data.getMonth()+1 ? `hoje cor_orgao __${context.orgao}` : null }` : null}
                  onClick={elem && elem.eventos.length > 0 ? this.getEventos(elem.dia, elem.eventos) : null}
                  >{elem ? elem.dia : null}</td>
                ))}</tr>
              )) : null}
            </tbody>
          </table>
        </div>
        <div>
          <span>{this.state.proxima_data ? this.state.proxima_data.dia : '...'}</span>
          <span className="caps">{meses[this.state.mes_referencia]}</span>
        </div>
        <div>
          {this.state.proximos_eventos.length > 0 ?
          <Carousel
            renderCenterLeftControls={({ previousSlide }) => (
              <button onClick={previousSlide}><LeftArrowAgendaCultural context={context} /></button>
            )}
            renderCenterRightControls={({ nextSlide }) => (
              <button onClick={nextSlide}><RightArrowAgendaCultural context={context} /></button>
            )}
          >
            {this.state.proximos_eventos.map( e => (
              <div>
                <Link to={`${context.orgao}/vem-ai/${e.post_name}`}>
                  <h3 className={`cor_orgao __${context.orgao}`}>{e.post_title}</h3>
                  <span>{e.data_extenso}</span>
                  <p className={`cor_orgao __${context.orgao}`}>{e.trecho}</p>
                </Link>
              </div>  
            ))}
          </Carousel> : 'Sem próximos eventos nesse mês'}
        </div>
      </section>
    )
  }
}
