import React, { Component } from 'react'
import OrgaoContext from '../../context'
import { Play } from '../own_icons'
import { Link } from 'react-router-dom'
import ReactHtmlParser from 'react-html-parser';
import ReactPlayer from 'react-player'
import Modal from '@material-ui/core/Modal';


export default class WidgetFicaDica extends Component {

  static contextType = OrgaoContext;

  state = {
    item : {
      title: {
        rendered: "Carregando..."
      },
      excerpt: {
        rendered: "Carregando..."
      },
      slug: ''
    },
    modalIsOpened: false
  }

  loadItems = () => {
    fetch(`${process.env.REACT_APP_WP_API}/wp-json/wp/v2/videos?per_page=1`).then(
      (response) => {
        return response.json() 
      }).then(
        r => {
          console.log("$$$ videos", r[0])
          this.setState({ item : r[0] });
    })
  }

  componentDidMount(){
    this.loadItems()
    console.log("$$$ teste")
  }

  handleOpen = e => {
    e.preventDefault();
    this.setState({ modalIsOpened: true });
  };

  handleClose = () => {
    this.setState({ modalIsOpened: false });
  };

  render(){
    const item = this.state.item,
    props = this.props,
    context = this.context

    return [
    <Modal
      id="modal_widget_videos" 
      open={this.state.modalIsOpened}
      onClose={this.handleClose}
      >
      {item.meta ? 
      <div class="modal_video_box">
      <ReactPlayer
        url={item.meta._video_url}
        controls={true}
      /></div> : <p>Sem vídeo</p> }
    </Modal>,
    <section id="videos" style={{ order: props.order }}>
      <div onClick={this.handleOpen}>
        <div className={`cortina bkg_orgao __${context.orgao}`}/>
        {item.meta ? 
        <ReactPlayer
          url={item.meta._video_url}
        /> : null }
      <div className={`fade __${context.orgao}`}/>
      <div className="conteudo">
        <Play />
        <h1>{ReactHtmlParser(item.title.rendered)}</h1>
        {ReactHtmlParser(item.excerpt.rendered)}
      </div>
      </div>
    </section>
    ]
  }
}
