import React, { Component } from 'react';

class PainelTempo extends Component {
  constructor(props) {
    super(props)

    this.state = {
      selectIsOpened: false,
      periodoAtual: null,
      ventoAtualString: null,
      data: {
        cidade: "Carregando...",
        backgroundImage: "url('img/painel-tempo/background/salvador.jpg')",
        previsao_hoje: {
          data: "...",
          grupo_clima: "...",
          icone: "img/painel-tempo/icones/indefinido.svg",
          min: null,
          max: null,
          iuv: null,
          ventos: [],
        },
        previsao_amanha: {
          data: "...",
          icone: "img/painel-tempo/icones/indefinido.svg",
          min: null,
          max: null
        },
        previsao_depois_amanha: {
          data: "...",
          icone: "img/painel-tempo/icones/indefinido.svg",
          min: null,
          max: null
        }
      },
      options: [
        {
          idlocalidade: null,
          cod_ibge: null,
          nome: "Carregando...",
          imagem: null
        }
      ],
      localizacao: {
        latitude: null,
        longitude: null
      }
    }

  }

  toggleSelectOption = () => {
    if (this.state.selectIsOpened) {
      this.setState({ selectIsOpened: false })
    } else {
      this.setState({ selectIsOpened: true })
    }
  }

  closeSelectOption = () => {
    if (this.state.selectIsOpened) {
      this.setState({ selectIsOpened: false })
    }
  }

  fetchData = (ibge) => {
    fetch(`${process.env.REACT_APP_API_PAINEL}/dados_painel/${ibge}`)
    .then((response) => {
      return response.text();
    })
    .then((myJson) => {      
      if(!JSON.parse(myJson).erro){
        console.log(JSON.parse(myJson))
        this.setState({ data: JSON.parse(myJson) }, () => this.ventoAtual());
      }else{
        this.loadData(); // Carrega Salvador
      }
    })
  }

  loadData = (e) => {
    // Verifica se existe numero de ibge ou se foi passado algum parâmetro
    if (e) {
      let ibge = e.target.dataset.ibge;
      this.fetchData(ibge);
      this.setState({ selectIsOpened : false})
    } else {
      // Caso negativo seleciona a localidade mais próxima ou Salvador como padrão (2927408)
      this.fetchData(this.getLocation());
    }
    
    // this.setState({ data })
  }

  getLocation = () => {
    if(this.state.localizacao.longitude){
    // Convertendo sinais - e . em strings para poder ser lido pelo servido php na url
    let longitude = this.state.localizacao.longitude.toString().replace('-', '(neg)').replace('.', '(dot)'),
    latitude = this.state.localizacao.latitude.toString().replace('-', '(neg)').replace('.', '(dot)');
    console.log(longitude, latitude);
    
    fetch(`${process.env.REACT_APP_API_PAINEL}/localizacao/${longitude}/${latitude}`)
    .then((response) => {
      return response.text();
    })
    .then((myJson) => {      
      if(!JSON.parse(myJson).cod_ibge){
        console.log(JSON.parse(myJson));
        
        return JSON.parse(myJson).cod_ibge;
      }else{
        return "2927408" // Carrega Salvador
      }
    })
  }

    return "2927408";
  }

  loadOptions = () => {
    fetch(`${process.env.REACT_APP_API_PAINEL}/cidades`)
      .then((response) => {
        return response.text();
      })
      .then((myJson) => {        
        this.setState({ options: JSON.parse(myJson) });
      });
  }

  turno = () => {
    if(!this.state.periodoAtual){
      console.log(Date.now());
      let d = new Date();
      let h = d.getHours();
      if(h > 18)  {this.setState({ periodoAtual : "Noite"})
      }else if(h < 12)  {this.setState({ periodoAtual : "Manhã"})}else{
        this.setState({ periodoAtual : "Tarde"});
      }
    }
  }

  ventoAtual(){    
    const ventos = this.state.data.previsao_hoje.ventos;
    
    ventos.forEach(elem => {
      console.log(elem.periodo, this.state.periodoAtual);
      
      if(elem.periodo === this.state.periodoAtual){        
        this.setState({ ventoAtualString : elem.velocidade_vento+" m/s "+elem.direcao_vento }, () => console.log(this.state.ventoAtualString))
      }
    });
  }

  async componentDidMount() {
    await this.loadOptions();
    await this.turno();

    if (window.location.protocol != 'https:'){
      this.loadData();
    }else{
      if (navigator.geolocation){ navigator.geolocation.getCurrentPosition( position => {   
        this.setState({ localizacao: position.coords }, this.loadData)
      })}
    }
  }

  render() {
    return (
      <section id="painel" style={{ order: this.props.order }}>      
      {/* Latitude {this.state.localizacao.latitude} <br/>
      Longitude {this.state.localizacao.longitude} <br/>
      Turno {this.state.periodoAtual} <br/> */}
      <section className="painel_tempo" style={{ backgroundImage: this.state.data.backgroundImage }}>
        <div className="gradiente" onClick={this.closeSelectOption}></div>
        <div className="main">
          <div className="select">
            <div className="select_arrow" onClick={this.toggleSelectOption}><img src="img/icones/seta-select-painel.svg" /></div>
            <div className="select_options" style={{ display: this.state.selectIsOpened ? "block" : "none" }}>
              <ul>
                {this.state.options.map((e) => <li onClick={this.loadData} data-ibge={e.cod_ibge} data-imagem={e.imagem}>{e.nome}</li>)}
              </ul>
            </div>
          </div>
          <header onClick={this.closeSelectOption}>
            <span className="cidade">{this.state.data.municipio}</span>
            <span className="divisoria"></span>
            <span className="data">{this.state.data.previsao_hoje.data}</span>
            <span className="grupotempo">{this.state.data.previsao_hoje.grupo_clima}</span>
          </header>
          <div className="previsao_hoje flex" onClick={this.closeSelectOption}>
            <div className="temperatura">
              <span className="icone"><img src={this.state.data.previsao_hoje.icone} /></span>
              <span className="max">{this.state.data.previsao_hoje.temperatura_max}°</span>
              <span className="min">/ {this.state.data.previsao_hoje.temperatura_min}°</span>
            </div>
            <div className="iuv_vento">
              {this.state.data.previsao_hoje.iuv ?
                <div>
                  <span className="icone"><img src="img/painel-tempo/icones/iuv.svg" /></span> <span>{this.state.data.previsao_hoje.iuv}</span>
                </div> : null
              }
              {this.state.data.previsao_hoje.ventos.length ?
                <div>
                  <span className="icone"><img src="img/painel-tempo/icones/ventos.svg" /></span> <span>{this.state.ventoAtualString}</span>
                </div> : null
              }
            </div>
          </div>
        </div>
        <div className="footer" onClick={this.closeSelectOption}>
          {this.state.data.previsao_amanha ?
            <div>
              <header>{this.state.data.previsao_amanha.data}</header>
              <div className="temperatura">
                <span className="icone"><img src={this.state.data.previsao_amanha.icone} /></span>
                <span className="max">{this.state.data.previsao_amanha.temperatura_max}°</span>
                <span className="min">/ {this.state.data.previsao_amanha.temperatura_min}°</span>
              </div>
            </div> : null
          }
          {this.state.data.previsao_depois_amanha ?
            <div>
              <header>{this.state.data.previsao_depois_amanha.data}</header>
              <div className="temperatura">
                <span className="icone"><img src={this.state.data.previsao_depois_amanha.icone} /></span>
                <span className="max">{this.state.data.previsao_depois_amanha.temperatura_max}°</span>
                <span className="min">/ {this.state.data.previsao_depois_amanha.temperatura_min}°</span>
              </div>
            </div> : null
          }
        </div>
      </section>
      </section>
    );
  }
}

export default PainelTempo;