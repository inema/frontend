import React, { Component } from 'react'
import Carousel from 'nuka-carousel';
import OrgaoContext from '../../context'
import { LeftArrowAgendaCultural, RightArrowAgendaCultural } from '../own_icons'

export default class Banners extends Component {

  static contextType = OrgaoContext;

  constructor(props) {
    super(props);

    this.state = {
      bannersSuperiores: [
        {}
      ],
      bannersInferiores: [
        {}
      ]
    }
  }

  loadBanners = () => {
    fetch(`${process.env.REACT_APP_WP_API}/wp-json/banners/v1/banners/`).then(
      (response) => response.json() ).then(
        (r) => {
          this.setState({ bannersSuperiores: r[0] }, console.log(r[0]));
          this.setState({ bannersInferiores: r[1] }, console.log(r[1]));
      })
  }

  componentDidMount() {
    this.loadBanners();
  }

  render() {
    const context = this.context;

    return (
      <section id="links_externos" style={{ order: this.props.order }}>
        <Carousel
          slidesToShow={1}
          withoutControls={true}
          pauseOnHover={true}
          autoplay={true}
          wrapAround={this.state.bannersSuperiores.length > 1}
        >
          {this.state.bannersSuperiores.map( v => (
            <a href={v.href} target="blank" className="banner_lateral_slide" >
              <div style={{
                backgroundImage: `url("${v.url}")`
              }}
            /></a>
          ))}
        </Carousel>
        <Carousel
          slidesToShow={1}
          withoutControls={true}
          pauseOnHover={true}
          autoplay={true}
          wrapAround={this.state.bannersInferiores.length > 1}
        >
          {this.state.bannersInferiores.map( v => (
            <a href={v.href} target="blank" className="banner_lateral_slide" >
              <div style={{
                backgroundImage: `url("${v.url}")`
              }}
            /></a>
          ))}
        </Carousel>
      </section>
    )
  }
}
