import React, { Component } from 'react'
import OrgaoContext from '../context'
import { Link } from "react-router-dom";

export default class MenuCaminho extends Component {

  static contextType = OrgaoContext;

  constructor(props, context) {
    super(props, context)
  }

  state = {
    // array contem dados da url divididas pelo slash "/"
    pathArray: [],
    loading: true,
    is404: false,
    // Futuramente pensar em um json ou banco de dados para catalogar essas páginas e caminhos
    // ou criar algum tipo de automaização de código que não tive tempo/esforço hábil para pensar
    // get slug, get title seria uma boa e simples, implementar em versões futuras, caso não exista descrever: 404 Essa página não existe
    listaPaginasLink: {
      sema: {
        url: `/sema`,
        nome: "Página principal"
      },
      inema: {
        url: `/inema`,
        nome: "Página principal"
      },
      galeria: {
        url: `/${this.context.orgao}/galeria`,
        nome: "Galeria"
      },
      "novos-colaboradores": {
        url: `/${this.context.orgao}/novos-colaboradores`,
        nome: "Colaboradores"
      },
      historico: {
        url: `/${this.context.orgao}/historico`,
        nome: "Histórico"
      },
      titulares: {
        url: `/${this.context.orgao}/titulares`,
        nome: "Titulares"
      },
      unidades: {
        url: `/${this.context.orgao}/unidades`,
        nome: "Unidades"
      },
      "orgaos-colegiados": {
        url: `/${this.context.orgao}/orgaos-colegiados`,
        nome: "Órgãos Colegiados"
      },
      "nosso-papel": {
        url: `/${this.context.orgao}/nosso-papel`,
        nome: "Nosso Papel"
      },
      "marcas-manuais": {
        url: `/${this.context.orgao}/marcas-manuais`,
        nome: "Marcas e Manuais"
      },
      "ver-de-dentro": {
        url: `/${this.context.orgao}/ver-de-dentro`,
        nome: "Ver de Dentro"
      },
      "altos-papos": {
        url: `/${this.context.orgao}/altos-papos`,
        nome: "Altos Papos"
      },
      "vem-ai": {
        url: `/${this.context.orgao}/vem-ai`,
        nome: "Vem Aí"
      },
      "destaque-do-mes": {
        url: `/${this.context.orgao}/destaque-do-mes`,
        nome: "Destaque do Mês"
      },
      "fica-a-dica": {
        url: `/${this.context.orgao}/fica-a-dica`,
        nome: "Fica a Dica"
      },
      "videos": {
        url: `/${this.context.orgao}/videos`,
        nome: "Vídeos"
      },
      "fale-conosco": {
        url: `/${this.context.orgao}/fale-conosco`,
        nome: "Fale Conosco"
      },
      "sites-relacionados": {
        url: `/${this.context.orgao}/sites-relacionados`,
        nome: "Sites Relacionados",
        categoria: "Consulta"
      },
      "noticias": {
        url: `/${this.context.orgao}/noticias`,
        nome: "Notícias",
      },
      "adf": {
        url: `#`,
        nome: "ADF",
      },
      "calendario": {
        url: `/${this.context.orgao}/adf/calendario`,
        nome: "Calendário",
      },
      "convocatorias": {
        url: `/${this.context.orgao}/adf/convocatorias`,
        nome: "Calendário",
      },
      "atas": {
        url: `/${this.context.orgao}/adf/atas`,
        nome: "Atas",
      },
      "manuais": {
        url: `/${this.context.orgao}/adf/manuais`,
        nome: "Manuais",
      },
      "relatorios_anuais": {
        url: `/${this.context.orgao}/adf/relatorios_anuais`,
        nome: "Relatórios Anuais",
      },
      "comunicados": {
        url: `/${this.context.orgao}/adf/comunicados`,
        nome: "Comunicados",
      },
      "legislacao-especifica": {
        url: `/${this.context.orgao}/adf/legislacao-especifica`,
        nome: "Legislação Específica",
      }
    },
    currentPage: [],
    currentContent: null
  }

  verifyPath = () => {
    let pathArray = this.state.pathArray;

    // Exclui caminhos que não estão na lista 
    pathArray.forEach((e, i, a) => {
      console.log(e)
      // Verifica se está na lista
      if (this.state.listaPaginasLink[e]) {
        let currentPage = this.state.currentPage;
        currentPage.push(this.state.listaPaginasLink[e])
        this.setState({
          currentPage,
          loading: false,
          is404: false
        })
      } else if (parseInt(e) > 0) {
        // Verifica se é um número != NaN, pq deve ser um ano de documentos por exemplo
        // Verifica se o item anterior era novos colaboradores, pois aí não deve mostrar número
        if(a[i-1] != "novos-colaboradores"){
          let currentPageTemp = {
            url: '#',
            nome: e
          }
          let currentPage = this.state.currentPage;
          currentPage.push(currentPageTemp)
          this.setState({
            loading: false,
            is404: false
          })
        }
      } else {
      console.log(e)
      // Caso não esteja, verifica se existe página com esse slug
      console.log(`${process.env.REACT_APP_WP_API}/wp-json/wp/v2/pages?slug=${e}`)
      fetch(`${process.env.REACT_APP_WP_API}/wp-json/wp/v2/pages?slug=${e}`).then(
        (response) => response.json()).then(
          (r) => {
            if (r.length) {
              let currentPageTemp = {
                url: `/${this.context.orgao}/${e}`,
                nome: r[0].title.rendered
              }
              this.setState({
                currentContent: r[0],
                loading: false,
                is404: false
              }, this.verifyCategory(currentPageTemp))
            } else {
              if (this.mounted) this.setState({
                loading: false,
                is404: true
              })
            }
          }
        )
    }
    })
}

verifyCategory = currentPageTemp => () => {
  console.log(this.state)

  // GET de dados de categoria
  fetch(`${process.env.REACT_APP_WP_API}/wp-json/wp/v2/categories/${this.state.currentContent.categories[0]}`).then(
    (response) => response.json()).then(
      (r) => {
        if (r.name) {
          let currentPage = this.state.currentPage;
          currentPageTemp.categoria = r.name
          currentPage.push(currentPageTemp)
          if (this.mounted) this.setState({ currentPage }, () => console.log(this.state))
        }
      }
    )
}

verifyContent = () => {
  if (this.state.is404) {
    this.props.is404()
  }
}

componentDidMount(){
  this.mounted = true;

  // Removendo primeiro item que sempre será em branco 
  let pathArray = window.location.pathname.split("/")
  pathArray.shift();

  // Caso seja posts, tipo notícias ele remove o último item, pelo menos temporariamente, futuramente associar já com o título
  if (this.props.post) pathArray.splice(-1, 1)

  if (this.mounted) this.setState({ pathArray }, this.verifyPath)

}

componentWillUnmount(){
  this.mounted = false;
}

render(){
  let loading = this.state.loading,
    is404 = this.state.is404;

  this.verifyContent()

  // Verifica se o caminho de alguma forma não foi encontro
  if (loading) {
    return (
      <div id="menu_caminho">
        <a href="#">Carregando...</a>
      </div>
    )
  } else if (is404) {
    return (
      <div id="menu_caminho">
        <a href="#">Erro 404 - Página não encontrada</a>
      </div>
    )
  } else {
    return (
      <div id="menu_caminho">
        {this.state.currentPage.map(e => {
          if (e.categoria) {
            return [<a href="#">{e.categoria}</a>, <Link to={e.url}>{e.nome}</Link>]
          } else {
            return <Link to={e.url}>{e.nome}</Link>
          }
        })}
      </div>
    )
  }
}
}
