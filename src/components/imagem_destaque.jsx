import React, { useContext } from 'react'
import OrgaoContext from '../context'

export default (props) => {

  const context = useContext(OrgaoContext);
  const imgUrl = context.json_data.better_featured_image ? context.json_data.better_featured_image.source_url : "";

  return (
    imgUrl ? <div className="img_destaque" style={{ backgroundImage: `url("${imgUrl}")` }}/> : null
  )
}