import React, { Component } from 'react'
import Carousel from 'nuka-carousel'
import MenuSistemas from '../components/menu_sistemas'
import OrgaoContext from '../context'
import ReactHtmlParser from 'react-html-parser';
import { Link } from "react-router-dom";


export default class Banner extends Component{

  static contextType = OrgaoContext;

  state = {
    banners : []
  }  

  loadBanners = () => {
    fetch(`${process.env.REACT_APP_WP_API}/wp-json/banner_principal/v1/banner_principal`).then(
      (response) => {
        return response.json() 
      } ).then(
        (r) => {
          console.log(r);
          this.setState({ banners : r });
      })
  }

  componentDidMount(){
    this.loadBanners()    
  }

  selectedAnimClass = () => {
    const animClasses = [ "fade_right", "just_fade", "fade_left" ]
    let min = 0;
    let max = animClasses.length;
    let randomInt = Math.floor(Math.random() * (max - min)) + min;
    console.log("random class ", animClasses[randomInt])
  }

  // código adaptado de https://awik.io/determine-color-bright-dark-using-javascript/
  isDark = arrayColor => {

    // Variables for red, green, blue values
    let r = arrayColor[0], 
    g = arrayColor[1],
    b = arrayColor[2], 
    hsp = 0;
    
    // HSP (Highly Sensitive Poo) equation from http://alienryderflex.com/hsp.html
    hsp = Math.sqrt(
    0.299 * (r * r) +
    0.587 * (g * g) +
    0.114 * (b * b)
    );

    // Using the HSP value, determine whether the color is light or dark
    if (hsp>127.5) {
      return false;
    }

    return true;
  }

  render(){

    const context = this.context;

    return (
      <div className={`${this.props.nobanner ? "nobanner" : null } banner_wrap`}>
        {this.props.nobanner ? null : (
          <Carousel 
          autoplay={true}
          autoplayInterval={6000}
          transitionMode={'scroll'} // 'scroll', 'fade', 'scroll3d'
          withoutControls={true}
          wrapAround={true}
          speed={500}
        >
          {this.state.banners.map( e => {
            let palette = e.palette[1]
            return ( <a href={e.href} target="_blank" >
              <div className="slide" style={{ backgroundImage: `url("${e.original}")` }}>
                <div className={`cortina __${context.orgao}`}></div>
                <div className={`texto __${this.isDark(palette) ? 'light' : 'dark' }`}><span style={{
                  backgroundColor: `rgb(${palette[0]},${palette[1]},${palette[2]})`,
                  boxShadow: `15px 0 0 rgb(${palette[0]},${palette[1]},${palette[2]}), -15px 0 0 rgb(${palette[0]},${palette[1]},${palette[2]})`
                }}>{e.texto}</span></div>
              </div>
            </a>
          )})}
          </Carousel>
        )}
        <MenuSistemas/>
      </div>
    )
  }
}
