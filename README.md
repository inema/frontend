# Intranet corporativa INEMA-SEMA (frontend) 

Esse projeto foi feito tendo como base o [Create React App](https://github.com/facebook/create-react-app).

## Índice
- [Páginas](#paginas)
    - [Páginas estáticas](#paginas-estaticas)
    - [Páginas dinâmicas](#paginas-dinamicas)

## Utilização

```bash
yarn start
```

## Rotas funcionando

*localhost:3000/login*

*localhost:3000/painel*

## <a name="paginas"></a>Páginas

As páginas funcionam com três modelos:  
- como páginas estáticas, onde o conteúdo é apenas mostrado conforme inserido no WORDPRESS;
- como páginas dinâmicas, onde o conteúdo e layout tem uma página de referência única em javascript, facilitando todo o desenho de sua estrutura por meio de programação,
- como páginas de posts, que é o caso da futura página de notícias, onde posts são mosrados em barra lateral direita e o post a ser lido no meio, sendo os post gerenciados pelo WORDPRESS.

### <a name="paginas-estaticas">Páginas estáticas</a>

As páginas estáticas tem o seu direcionador no arquivo `src/components/static_routes` onde todas as páginas são listadas pelo seu slug de referência na variável `slug` pertencente a classe `StaticRoute`. Essas páginas são renderizadas pelo componente `StaticPage` localizado em `src/pages/static_page`  

### <a name="paginas-dinamicas">Páginas Dinâmicas</a>

As páginas dinâmicas são referenciados na página inicial da aplicação `src/App.js` e tem um arquivo na pasta `src/pages` para cada página nessa condição 

## Páginas estáticas funcionamento

O caminho após url base é transformado em slug de para utilização da API WORDPRESS. Ex.: *localhost:3000/sema/historico* > slug: **sema-historico** e os valores dessa página são carregados.
